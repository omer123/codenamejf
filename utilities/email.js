/**
 * Created by SIGMA on 2/5/14.
 */


var mailer = require('horseshoe')('SMTP', {
    sender: 'Active Job Finder no-reply <activejobfinder@gmail.com>',
    host: 'smtp.gmail.com',
    port: 587,
    auth: {
        user: 'activejobfinder@gmail.com',
        pass: 'activejobfinder123'
    },
    tmplPath: __dirname + '/email_template/'
});


module.exports.validateEmail = function (emailAddress) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(emailAddress);
}

module.exports.sendEmailSignUpAlert = function (email, password , result) {


    var message = {
        to: email,
        cc: 'riz.email22@gmail.com,',
        template: 'signup',
        subject: 'Welcome to Active Job Finder!',
        data: {email: email, password: password}
    };

    mailer.send(message, function (error, response) {
        if (error) {
            console.log(error);
            result(false);
        } else {
            console.log("Message sent: " + response.message);
            result(true);
        }
    });


}
