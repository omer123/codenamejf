/**
 * Created by SIGMA on 2/23/14.
 */

var mongoose = require('mongoose');
var async = require('async');
var fs = require('fs');
var pathdir = require('path');
//var easyimg = require('easyimage');

var itemsPath = __dirname + "/images/items";
var companyPath = __dirname + "/images/companies";
var adPath = __dirname + "/images/ads";
var emailPath = __dirname + "/images/emails";


module.exports.emailImageWithFileName = function (req, res) {
    var filename = req.params.filename;
    var imageFile = null;
    var exitFlag = false;

    //////  ************** NEW ASYNC METHOD
    fs.readdir(emailPath, function (err, imgfiles) {
        if (!err) {
            async.forEach(imgfiles, function (imgFile, callback) {
                    if (req.params.filename == imgFile) {
                        fs.readFile(emailPath + "/" + filename, function (err, data) {
                            if (!err) {

                                exitFlag = true;
                                console.log('File Found');
                                imageFile = data;
                                callback();

                            }
                            else {
                                callback(err);
                            }
                        });
                    }
                    else {
                        callback();
                    }

                },
                function (err) {
                    if (err) {
                        res.setHeader('Response-Description', err);
                        res.statusCode = 404;
                        res.end();
                    }
                    if (imageFile == null) {

                        res.statusCode = 404;
                        res.end();
                    }
                    else {
                        res.writeHead(200, {'Content-Type': 'image/png' });
                        res.end(imageFile);

                    }
                });

        } else {
            res.send(500);
        }
    });
}




function getExtension(filename) {
    var i = filename.lastIndexOf('.');
    return (i < 0) ? '' : filename.substr(i);
}
//*********************************** DEPRICATED FUNCTIONS START***************************//
module.exports.getExtension = function getExtension(filename) {
    var i = filename.lastIndexOf('.');
    return (i < 0) ? '' : filename.substr(i);
}
module.exports.itemMultiImage = function (req, res) {
    var itemid = req.params.itemid;
    var imageFile = null;
    var extensionList = [];
    var exitFlag = false;
//////  ************** NEW ASYNC METHOD
    fs.readdir(itemsPath, function (err, files) {
        if (!err) {
            async.forEach(files, function (file, callback) {
                    if (file == itemid) {
//                    console.log("Item ID : " + itemid);
                        fs.readdir(itemsPath + "/" + itemid, function (err, imgfiles) {
//                        console.log('Items Image Files : ' + imgfiles);
                            if (!err) {
                                console.log('All Images Items: ' + imgfiles);
//                                console.log(itemsPath + "/" + itemid + "/" + imgfiles[0]);

                                imageFile = imgfiles;

                                var totalFiles = imgfiles.length;

                                for (i = 0; i < totalFiles; i++) {
                                    var extension = getExtension(imgfiles[i]);
                                    console.log('Image File Name : ' + imgfiles[i]);
                                    console.log('Image File Extension  : ' + extension);
                                    if (extension == '.jpeg' || extension == '.jpg' ||
                                        extension == '.gif' || extension == '.png') {
//                                        extensionList.push(extension);

                                        if (req.params.filename == imgfiles[i]) {
                                            fs.readFile(itemsPath + "/" + itemid + "/" + imgfiles[i], function (err, data) {
                                                if (!err) {

                                                    exitFlag = true;
//                                        res.writeHead(200, {'Content-Type': 'image/png' });
//                                        res.end(data);
                                                    imageFile = data;
                                                    callback();
                                                }
                                                else {
                                                    callback(err);
                                                }
                                            });
                                        }
                                    }
                                }


//                                fs.readFile(itemsPath + "/" + itemid + "/" + imgfiles[0], function (err, data) {
//                                    if (!err) {
//
//                                        exitFlag = true;
////                                        res.writeHead(200, {'Content-Type': 'image/png' });
////                                        res.end(data);
//                                        imageFile = data;
//                                        callback();
//                                    }
//                                    else {
//                                        callback(err);
//                                    }
//                                });
                            }
                            else {
                                callback(err);
                            }
                        });
                    }
                    else {
                        callback();
                    }
                },
                function (err) {
                    if (err) next(err);
                    if (imageFile == null) {

                        res.statusCode = 404;
                        res.end();
                    }
                    else {
//                        res.json(extensionList);
//                        res.json(imageFile);
                        res.writeHead(200, {'Content-Type': 'image/png' });
                        res.end(imageFile);
                    }
                });
        } else {
            res.send(500);
        }
    });
}
module.exports.itemImageWithFileName = function (req, res) {
    var itemid = req.params.itemid;
    var imageFile = null;
    var exitFlag = false;

    //////  ************** NEW ASYNC METHOD
    fs.readdir(itemsPath, function (err, files) {
        if (!err) {
            async.forEach(files, function (file, callback) {
                    if (file == itemid) {
//                    console.log("Item ID : " + itemid);
                        fs.readdir(itemsPath + "/" + itemid, function (err, imgfiles) {
//                        console.log('Items Image Files : ' + imgfiles);
                            if (!err) {
                                console.log('All Images Items: ' + imgfiles);
                                console.log(itemsPath + "/" + itemid + "/" + req.params.filename);

                                async.forEach(imgfiles, function (imgFile, callback) {
                                        if (req.params.filename == imgFile) {
                                            fs.readFile(itemsPath + "/" + itemid + "/" + req.params.filename, function (err, data) {
                                                if (!err) {

                                                    exitFlag = true;
                                                    console.log('File Found');
                                                    imageFile = data;
                                                    callback();

                                                }
                                                else {
                                                    callback(err);
                                                }
                                            });
                                        }
                                        else {
                                            callback();
                                        }

                                    },
                                    function (err) {
                                        if (err) callback(err);
                                        callback();
                                    });
                            }
                            else {
                                callback(err);
                            }
                        });
                    }
                    else {
                        callback();
                    }
                },
                function (err) {
                    if (err) next(err);
                    if (imageFile == null) {

                        res.statusCode = 404;
                        res.end();
                    }
                    else {
                        res.writeHead(200, {'Content-Type': 'image/png' });
                        res.end(imageFile);

                    }
                });
        } else {
            res.send(500);
        }
    });
}
module.exports.scanMultiImages = function (imageURL, itemid) {

    var imageFile = null;
    var imageResultList = [];
//////  ************** NEW ASYNC METHOD
    fs.readdir(itemsPath, function (err, files) {
        if (!err) {
            async.forEach(files, function (file, callback) {
                    if (file == itemid) {
//                    console.log("Item ID : " + itemid);
                        fs.readdir(itemsPath + "/" + itemid, function (err, imgfiles) {
//                        console.log('Items Image Files : ' + imgfiles);
                            if (!err) {
                                console.log('All Images Items: ' + imgfiles);
//                                console.log(itemsPath + "/" + itemid + "/" + imgfiles[0]);

                                imageFile = imgfiles;

                                var totalFiles = imgfiles.length;

                                for (i = 0; i < totalFiles; i++) {
                                    var extension = getExtension(imgfiles[i]);
                                    console.log('Image File Name : ' + imgfiles[i]);
                                    console.log('Image File Extension  : ' + extension);
                                    if (extension == '.jpeg' || extension == '.jpg' ||
                                        extension == '.gif' || extension == '.png') {
                                        imageResultList.push(imageURL + itemid + '/' + imgfiles[i]);
//                                        callback();
                                    }
                                }
                                callback();
                            }
                            else {
                                callback(err);
                            }
                        });
                    }
                    else {
                        callback();
                    }
                },
                function (err) {
                    if (err) next(err);
                    if (imageFile == null) {
                        return imageResultList;
                    }
                    else {
                        return imageResultList;
                    }
                });
        } else {
            return imageResultList;
        }
    });
}
//*********************************** DEPRICATED FUNCTIONS END ***************************//

// Make Dir Function
fs.mkdirParent = function (dirPath, mode, callback) {
    //Call the standard fs.mkdir
    fs.mkdir(dirPath, mode, function (error) {
        //When it fail in this way, do the custom steps
        if (error && error.errno === 34) {
            //Create all the parents recursively
            fs.mkdirParent(pathdir.dirname(dirPath), mode, callback);
            //And then the directory
            fs.mkdirParent(dirPath, mode, callback);
        }
        //Manually run the callback since we used our own callback to do all these
        callback && callback(error);
    });
};

// Company Functions
module.exports.saveCompanyImageWeb = function (req, res, companyid) {

    //var companyid = companyid;


    if (!fs.existsSync(companyPath + "/" + companyid)) {
        fs.mkdirParent(companyPath + "/" + companyid);
    }
    fs.readFile(req.files.logo_image.path, function (err, data) {
        var newPath = companyPath + "/" + companyid + "/" + req.files.logo_image.name;
        fs.writeFile(newPath, data, function (err) {
            if (!err) {
                req.flash('success', "Registration Successful!");
                return res.redirect('/');
//                res.statusCode = 200;
//                res.end();f
            }
            else {
                req.flash('error', JSON.stringify(err));
                return res.redirect('/');
//                console.log(err);
//                res.statusCode = 500;
//                res.end();
            }
        });
    });
}
module.exports.companyImage = function (req, res) {

    var userid = req.params.companyname;
    //userid = userid.toLowerCase();
    var imageFile = null;



    //////  ************** NEW ASYNC METHOD
    fs.readdir(companyPath, function (err, files) {
        if (!err) {
            async.forEach(files, function (file, callback) {
                    if (file == userid) {
//                    console.log("Item ID : " + userid);
                        fs.readdir(companyPath + "/" + userid, function (err, imgfiles) {
//                        console.log('Items Image Files : ' + imgfiles);
                            if (!err) {
                                console.log('All Images Users: ' + imgfiles);


                                var totalFiles = imgfiles.length;

                                for (var i = 0; i < totalFiles; i++) {
                                    var extension = getExtension(imgfiles[i]);
                                    console.log('Image File Name : ' + imgfiles[i]);
                                    console.log('Image File Extension  : ' + extension);
                                    if (extension == '.jpeg' || extension == '.jpg' ||
                                        extension == '.gif' || extension == '.png') {
                                        fs.readFile(companyPath + "/" + userid + "/" + imgfiles[i], function (err, data) {
                                            exitFlag = true;
                                            imageFile = data;
                                            callback();
                                        });
                                    }
                                }
                            }
                            else {
                                callback(err);
                            }
                        });
                    }
                    else {
                        callback();
                    }
                },
                function (err) {
                    if (err) next(err);
                    if (imageFile == null) {

                        res.statusCode = 404;
                        res.end();
                    }
                    else {
                        res.writeHead(200, {'Content-Type': 'image/png' });
                        res.end(imageFile);

                    }
                });
        } else {
            res.send(500);
        }
    });
}

// Ads Functions
module.exports.saveAdImageWeb = function (req, res) {

    var ad_id = req.body.ad_id;


    if (!fs.existsSync(adPath + "/" + ad_id)) {
        fs.mkdirParent(adPath + "/" + ad_id);
    }
    fs.readFile(req.files.ad_image.path, function (err, data) {
        var newPath = adPath + "/" + ad_id + "/" + req.files.ad_image.name;
        fs.writeFile(newPath, data, function (err) {
            if (!err) {
                req.flash('success', "Ad Created Successfully!");
                return res.redirect('/');
//                res.statusCode = 200;
//                res.end();f
            }
            else {
                req.flash('error', JSON.stringify(err));
                return res.redirect('/');
//                console.log(err);
//                res.statusCode = 500;
//                res.end();
            }
        });
    });
}
module.exports.adImage = function (req, res) {

    var userid = req.params.adid;
    var imageFile = null;



    //////  ************** NEW ASYNC METHOD
    fs.readdir(adPath, function (err, files) {
        if (!err) {
            async.forEach(files, function (file, callback) {
                    if (file == userid) {
//                    console.log("Item ID : " + userid);
                        fs.readdir(adPath + "/" + userid, function (err, imgfiles) {
//                        console.log('Items Image Files : ' + imgfiles);
                            if (!err) {
                                console.log('All Images Users: ' + imgfiles);
                                var totalFiles = imgfiles.length;

                                for (var i = 0; i < totalFiles; i++) {
                                    var extension = getExtension(imgfiles[i]);
                                    console.log('Image File Name : ' + imgfiles[i]);
                                    console.log('Image File Extension  : ' + extension);
                                    if (extension == '.jpeg' || extension == '.jpg' ||
                                        extension == '.gif' || extension == '.png') {
                                        fs.readFile(adPath + "/" + userid + "/" + imgfiles[i], function (err, data) {
                                            exitFlag = true;
                                            imageFile = data;
                                            callback();
                                        });
                                    }
                                }
                            }
                            else {
                                callback(err);
                            }
                        });
                    }
                    else {
                        callback();
                    }
                },
                function (err) {
                    if (err) next(err);
                    if (imageFile == null) {

                        res.statusCode = 404;
                        res.end();
                    }
                    else {
                        res.writeHead(200, {'Content-Type': 'image/png' });
                        res.end(imageFile);

                    }
                });
        } else {
            res.send(500);
        }
    });
}

