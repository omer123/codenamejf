/**
 * Created by SIGMA on 2/23/14.
 */

var mongoose = require('mongoose');
var async = require('async');
var random = require('../utilities/random');
var fs = require('fs');
var pathdir = require('path');
var cvPath = __dirname + "/cv/";
var app_config = require('../config/app_config');


function getExtension(filename) {
    var i = filename.lastIndexOf('.');
    return (i < 0) ? '' : filename.substr(i);
}

// Make Dir Function
fs.mkdirParent = function (dirPath, mode, callback) {
    //Call the standard fs.mkdir
    fs.mkdir(dirPath, mode, function (error) {
        //When it fail in this way, do the custom steps
        if (error && error.errno === 34) {
            //Create all the parents recursively
            fs.mkdirParent(pathdir.dirname(dirPath), mode, callback);
            //And then the directory
            fs.mkdirParent(dirPath, mode, callback);
        }
        //Manually run the callback since we used our own callback to do all these
        callback && callback(error);
    });
};

// Ads Functions
module.exports.uploadCV = function (req, res) {

    var randomIdForFolderGeneration = random.randomGUID();


    if (!fs.existsSync(cvPath + randomIdForFolderGeneration)) {
        fs.mkdirParent(cvPath + randomIdForFolderGeneration);
    }
    fs.readFile(req.files.cv.path, function (err, data) {
        var newPath = cvPath + randomIdForFolderGeneration + "/" + req.files.cv.name;
        fs.writeFile(newPath, data, function (err) {
            if (!err) {
                res.json({
                    success: 1,
                    error: null,
                    FileURL: app_config.cvFileBaseURL + randomIdForFolderGeneration
                });
            }
            else {
//                console.log(err);
                res.statusCode = 500;
                res.json({
                    success: 0,
                    error: err,
                    FileURL: null
                });
            }
        });
    });
}
module.exports.downloadCV = function (req, res) {

    var cvid = req.params.cvId;
    var cvFile = null;


    //////  ************** NEW ASYNC METHOD
    fs.readdir(adPath, function (err, files) {
        if (!err) {
            async.forEach(files, function (file, callback) {
                    if (file == cvid) {
//                    console.log("Item ID : " + cvid);
                        fs.readdir(cvPath + cvid, function (err, cvfiles) {
//                        console.log('Items Image Files : ' + cvfiles);
                            if (!err) {
                                console.log('All Images Users: ' + cvfiles);
                                var totalFiles = cvfiles.length;

                                for (i = 0; i < totalFiles; i++) {
                                    var extension = getExtension(cvfiles[i]);
                                    console.log('  File Name : ' + cvfiles[i]);
                                    console.log('  File Extension  : ' + extension);
                                    if (extension == '.docx' || extension == '.doc' ||
                                        extension == '.pdf' || extension == '.txt'   ) {
                                        fs.readFile(cvPath + cvid + "/" + cvfiles[i], function (err, data) {
                                            exitFlag = true;
                                            cvFile = data;
                                            callback();
                                        });
                                    }
                                }
                            }
                            else {
                                callback(err);
                            }
                        });
                    }
                    else {
                        callback();
                    }
                },
                function (err) {
                    if (err) next(err);
                    if (cvfiles == null) {

                        res.statusCode = 404;
                        res.end();
                    }
                    else {
//                        res.writeHead(200, {'Content-Type': 'image/png' });
//                        res.end(cvFile);
                        res.sendfile(cvFile);

                    }
                });
        } else {
            res.send(500);
        }
    });
}

