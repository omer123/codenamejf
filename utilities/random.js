/**
 * Created by SIGMA on 2/5/14.
 */

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
};

function s3() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(8)
        .substring(1);
};

module.exports.randomGUID = function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

module.exports.randomPassToken = function guid() {
    return s3() + s3();
}