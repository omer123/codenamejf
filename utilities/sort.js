/**
 * Created by Rizwan on 11/15/14.
 */

//function compare(a,b) {
//    if (a.last_nom < b.last_nom)
//        return -1;
//    if (a.last_nom > b.last_nom)
//        return 1;
//    return 0;
//}
//
//objs.sort(compare);


module.exports.asc = function (array, key ){

    function compare(a,b) {
        if (a[key] < b[key])
            return -1;
        if (a[key] > b[key])
            return 1;
        return 0;
    }

    array.sort(compare);

};


module.exports.desc = function (array, key ){

    function compare(a,b) {
        if (a[key] > b[key])
            return -1;
        if (a[key] < b[key])
            return 1;
        return 0;
    }

    array.sort(compare);

};