var mongoose = require('mongoose')
    , JobType = require('../models/JobTypeModel')
    , Currency = require('../models/CurrencyModel')
    , random = require('../utilities/random')
    , async = require('async');


// Get All
exports.getAll = function (req, res) {
    JobType.find().sort('description').exec(function (err, JobTypeFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            res.json(JobTypeFromRepo);
        }
    });
}