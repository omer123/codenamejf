var mongoose = require('mongoose')
    , Country = require('../models/CountryModel')
    , Currency = require('../models/CurrencyModel')
    , random = require('../utilities/random')
    , async = require('async')
    , changeCase = require('change-case');


// Get All
exports.getAll = function (req, res) {
    Country.find().sort('countryname').exec(function (err, CountryFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            for(var i=0; i<CountryFromRepo.length; i++){
                CountryFromRepo[i].countryname = CountryFromRepo[i].countryname.toUpperCase();//changeCase.upperCaseFirst(CountryFromRepo[i].countryname);
            }
            res.json(CountryFromRepo);
        }
    });
}