var mongoose = require('mongoose')
    , City = require('../models/CityModel')
    , Currency = require('../models/CurrencyModel')
    , random = require('../utilities/random')
    , async = require('async');


// Get All
exports.getAll = function (req, res) {
    City.find().sort('cityname').exec(function (err, CityFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            res.json(CityFromRepo);
        }
    });
}

exports.getAllByRegionId = function (req, res) {
    City.find({region_id: req.params.region_id}).sort('cityname').exec(function (err, CityFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            res.json(CityFromRepo);
        }
    });
}