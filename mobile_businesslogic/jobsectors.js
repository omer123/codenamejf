var mongoose = require('mongoose')
    , JobSector = require('../models/JobSectorModel')
    , Currency = require('../models/CurrencyModel')
    , random = require('../utilities/random')
    , async = require('async');


// Get All
exports.getAll = function (req, res) {
    JobSector.find().sort('businesssector').exec(function (err, CountryFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            res.json(CountryFromRepo);
        }
    });
}