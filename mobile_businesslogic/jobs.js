var mongoose = require('mongoose')
    , Job = require('../models/JobModel')
    , JobViewed = require('../models/JobViewedModel')
    , JobApplied = require('../models/JobAppliedModel')
    , JobSearch = require('../models/JobSearchModel')
    , JobPost = require('../models/JobPostModel')
    , Currency = require('../models/CurrencyModel')
    , random = require('../utilities/random')
    , Company = require('../models/CompanyModel')
    , app_config = require('../config/app_config')
    , User = require('../models/UserModel')
    , TechnicalSkill = require('../models/TechnicalSkillModel')
    , Tool = require('../models/ToolModel')
    , JobType = require('../models/JobTypeModel')
    , async = require('async');


// Get All
exports.getAll = function (req, res) {
    Job.find().sort('title').exec(function (err, JobFromRepoList) {
        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {
            res.json(JobFromRepoList);
        }
    });
}


// Get Job by ID
exports.getById = function (req, res) {
    Job.findOne({job_id: req.params.job_id}).sort('title').exec(function (err, JobFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {

            JobPost.findOne({job_id: JobFromRepo.job_id}, function (err, jobPostFromRepo) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                }
                else {
                    if (jobPostFromRepo) {
                        User.findOne({username: jobPostFromRepo.poster_id}, function (err, userFromRepo) {
                            if (err) {
                                res.statusCode = 500;
                                res.send(err);
                            }
                            else {
                                if (userFromRepo) {

                                    Company.findOne({company_id: userFromRepo.company_id}, function (err, companyFromRepo) {
                                        if (err) {
                                            res.statusCode = 500;
                                            res.send(err);
                                        }
                                        else {
                                            if (companyFromRepo) {
                                                Currency.findOne({currency_id: JobFromRepo.salary_currency_id}, function (err, currencyFromRepo) {
                                                    if (err) {
                                                        res.statusCode = 500;
                                                        res.send(err);
                                                    }
                                                    else {
                                                        if (currencyFromRepo) {
                                                            JobType.findOne({jobtype_id: JobFromRepo.jobtype_id}, function (err, jobTypeFromRepo) {
                                                                if (err) {
                                                                    res.statusCode = 500;
                                                                    res.send(err);
                                                                }
                                                                else {
                                                                    if (jobTypeFromRepo) {
                                                                        var newJobViewedObj = new JobViewed();
                                                                        newJobViewedObj.jobviewed_id = random.randomGUID();
                                                                        newJobViewedObj.job_id = req.params.job_id;
                                                                        newJobViewedObj.jobsearchid = req.params.jobsearchid;

                                                                        JobViewed.findOne({job_id: newJobViewedObj.job_id, jobsearchid : req.params.jobsearchid}, function (err, jobViewedFromRepo) {
                                                                            if (err) {
                                                                                res.statusCode = 500;
                                                                                res.send(err);
                                                                            }
                                                                            else {
                                                                                if (jobViewedFromRepo) {
                                                                                    console.log('Job Already Viewed');
                                                                                    JobFromRepo.salary = currencyFromRepo.sign + ' ' + JobFromRepo.salary_low + ' - ' + currencyFromRepo.sign + ' ' + JobFromRepo.salary_high;
                                                                                    JobFromRepo.jobtype = jobTypeFromRepo.description;
                                                                                    JobFromRepo.company = companyFromRepo.companyname;
                                                                                    var jobDetails = {jobDetails: JobFromRepo};
                                                                                    res.json(jobDetails);
                                                                                }
                                                                                else {
                                                                                    newJobViewedObj.save(function (err) {
                                                                                        if (err) {
                                                                                            res.statusCode = 500;
                                                                                            res.send(err);
                                                                                        }
                                                                                        else {

                                                                                            JobFromRepo.salary = JobFromRepo.salary_low + ' - ' + JobFromRepo.salary_high;
                                                                                            JobFromRepo.jobtype = jobTypeFromRepo.description;
                                                                                            JobFromRepo.company = companyFromRepo.companyname;
                                                                                            var jobDetails = {jobDetails: JobFromRepo};
                                                                                            res.json(jobDetails);
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                    else {
                                                                        var jobDetails = {jobDetails: JobFromRepo};
                                                                        res.json(jobDetails);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            var jobDetails = {jobDetails: JobFromRepo};
                                                            res.json(jobDetails);
                                                        }
                                                    }
                                                });
                                            }
                                            else {

                                                var jobDetails = {jobDetails: JobFromRepo};
                                                res.json(jobDetails);
                                            }
                                        }

                                    });
                                }
                                else {

                                    var jobDetails = {jobDetails: JobFromRepo};
                                    res.json(jobDetails);
                                }
                            }

                        });
                    }
                    else {
                        var jobDetails = {jobDetails: JobFromRepo};
                        res.json(jobDetails);
                    }
                }
            });
        }
    });
}

// Get All Job Posts
exports.getAllJobPosts = function (req, res) {
    JobPost.find().sort('end_date').exec(function (err, JobPostsFromRepoList) {
        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {
            res.json(JobPostsFromRepoList);
        }
    });
}

// Get All Job Posts
exports.getAllJobPostsAndJobDetail = function (req, res) {

    var jobsResponseArray = [];
    var jobsResponseObj = {};

    JobPost.find(function (err, jobPostsFromRepoList) {

        async.forEach(jobPostsFromRepoList, function (jobPostsFromRepo, callback) {

                Job.findOne({job_id: jobPostsFromRepo.job_id}, function (err, jobFromRepo) {
                    if (err) {
                        callback(err);
                    }
                    else {
                        jobsResponseObj.JobPost = jobPostsFromRepo;
                        jobsResponseObj.JobDetail = jobFromRepo;

                        jobsResponseArray.push(jobsResponseObj);
                        callback();
                    }
                });


            },
            function (err) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                } else {
                    res.json(jobsResponseArray);
                }
            });
    });
}

// Get Job Posts
exports.getJobPostsByCompany = function (req, res) {

    var jobsResponseArray = [];
    var jobsResponseObj = {};

    User.find({company_id: req.params.company_id}, function (err, usersFromRepoList) {
        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {

            var usernames = [];
            for (var i = 0; i < usersFromRepoList.length; i++) {
                usernames.push(usersFromRepoList.username);
            }

            JobPost.find({poster_id: {$in: usernames}}, function (err, jobPostsFromRepoList) {

                async.forEach(jobPostsFromRepoList, function (jobPostsFromRepo, callback) {

                        Job.findOne({job_id: jobPostsFromRepo.job_id}, function (err, jobFromRepo) {
                            if (err) {
                                callback(err);
                            }
                            else {
                                jobsResponseObj.JobPost = jobPostsFromRepo;
                                jobsResponseObj.JobDetail = jobFromRepo;

                                jobsResponseArray.push(jobsResponseObj);
                                callback();
                            }
                        });


                    },
                    function (err) {
                        if (err) {
                            res.statusCode = 500;
                            res.send(err);
                        } else {
                            res.json(jobsResponseArray);
                        }
                    });
            });
        }
    });
}

// Get All Skills
exports.getAllJobTechnicalSkills = function (req, res) {
    TechnicalSkill.find().sort('skillname').exec(function (err, skillRepoList) {
        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {
            res.json(skillRepoList);
        }
    });
}

// Get All Tools
exports.getAllJobTools = function (req, res) {
    Tool.find().sort('toolname').exec(function (err, toolsFromRepoList) {
        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {
            res.json(toolsFromRepoList);
        }
    });
}

// Get   Skill By ID
exports.getAllJobTechnicalSkillById = function (req, res) {
    TechnicalSkill.findOne({tool_id: req.params.id}).sort('skillname').exec(function (err, skillRepoList) {
        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {
            res.json(skillRepoList);
        }
    });
}

// Get   Tool By ID
exports.getAllJobToolById = function (req, res) {
    Tool.findOne({tool_id: req.params.id}).sort('toolname').exec(function (err, toolsFromRepoList) {
        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {
            res.json(toolsFromRepoList);
        }
    });
}

// Search Jobs on the Basis of Search Criteria
exports.getJobsFromJobSearch = function (req, res) {

    var responseArray = [];
    var responseObj = {};

    JobSearch.findOne({jobsearchid: req.params.jobsearchid}, function (err, jobSearchFromRepo) {
            if (err) {
                res.statusCode = 500;
                res.send(err);
            }
            else {
                if (jobSearchFromRepo) {
                    searchReq = {};

                    if (jobSearchFromRepo.country_id == null || jobSearchFromRepo.country_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter country id");
                    }
                    else if (jobSearchFromRepo.region_id == null || jobSearchFromRepo.region_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter region id");
                    }
                    else if (jobSearchFromRepo.jobsector_id == null || jobSearchFromRepo.jobsector_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter job sector id");
                    }
                    else if (jobSearchFromRepo.jobtype_id == null || jobSearchFromRepo.jobtype_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter job type id");
                    }
                    else {
                        if (jobSearchFromRepo.city_id == null || jobSearchFromRepo.city_id == "") {
                            searchReq = {
                                country_id: jobSearchFromRepo.country_id,
                                region_id: jobSearchFromRepo.region_id,
                                jobsector_id: jobSearchFromRepo.jobsector_id
                            }
                        }
                        else if (jobSearchFromRepo.city_id != null) {
                            searchReq = {
                                country_id: jobSearchFromRepo.country_id,
                                region_id: jobSearchFromRepo.region_id,
                                city_id: jobSearchFromRepo.city_id,
                                jobsector_id: jobSearchFromRepo.jobsector_id
                            }
                        }
                        console.log(searchReq);

                        Company.find(searchReq, function (err, companiesList) {
                            console.log(companiesList);
                            if (err) {
                                res.statusCode = 500;
                                res.send(err);
                            }
                            else if (companiesList.length > 0) {

                                // Testing

                                async.forEach(companiesList, function (companyFromRepo, callback) {

                                        User.findOne({company_id: companyFromRepo.company_id}, function (err, userFromRepo) {
                                            if (err) {
                                                callback(err);
                                            }
                                            else {
                                                if (userFromRepo) {
                                                    JobPost.find({poster_id: userFromRepo.username}, function (err, jobPostsFromRepoList) {
                                                        console.log('JOB POSTS \n ' + jobPostsFromRepoList);
                                                        if (err) {
                                                            callback(err);
                                                        }
                                                        else {
                                                            if (jobPostsFromRepoList.length > 0) {

                                                                async.forEach(jobPostsFromRepoList, function (jobPostsFromRepo, callback) {

                                                                        console.log('JOB ID : ' + jobPostsFromRepo.job_id);
                                                                        console.log('JOBTYPEID :  ' + jobSearchFromRepo.jobtype_id);
                                                                        Job.findOne({
                                                                            job_id: jobPostsFromRepo.job_id,
                                                                            jobtype_id: jobSearchFromRepo.jobtype_id
                                                                        }, function (err, jobFromRepo) {
                                                                            console.log('JOBs \n ' + jobFromRepo);
                                                                            if (err) {
                                                                                callback(err);
                                                                            }
                                                                            else if (jobFromRepo) {

                                                                                JobType.findOne({jobtype_id: jobFromRepo.jobtype_id}).sort('description').exec(function (err, jobTypeFromRepo) {
                                                                                    if (err) {
                                                                                        callback(err);
                                                                                    }
                                                                                    else {

                                                                                        JobApplied.find({jobsearchid: req.params.jobsearchid}, function (err, jobsAppliedFromRepoList) {
                                                                                            if (err) {
                                                                                                callback(err);
                                                                                            }
                                                                                            else if (jobsAppliedFromRepoList.length > 0) {

                                                                                                for (var i = 0; i < jobsAppliedFromRepoList.length; i++) {
                                                                                                    if (jobsAppliedFromRepoList[i].job_id == jobFromRepo.job_id) {
                                                                                                        callback();
                                                                                                    }
                                                                                                    else {
                                                                                                        responseObj = {};
                                                                                                        responseObj.jobId = jobFromRepo.job_id;
                                                                                                        responseObj.jobTitle = jobFromRepo.title;
                                                                                                        responseObj.company_logo = app_config.companyLogoBaseURL + companyFromRepo.companyname.toLowerCase();
                                                                                                        responseObj.jobtype = jobTypeFromRepo.type;
                                                                                                        responseObj.jobtype_id = jobFromRepo.jobtype_id;
                                                                                                        responseObj.jobtype_description = jobTypeFromRepo.description;
                                                                                                        responseObj.post_date = jobPostsFromRepo.post_date;

                                                                                                        responseArray.push(responseObj);
                                                                                                        callback();
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            else {
                                                                                                responseObj = {};
                                                                                                responseObj.jobId = jobFromRepo.job_id;
                                                                                                responseObj.jobTitle = jobFromRepo.title;
                                                                                                responseObj.company_logo = app_config.companyLogoBaseURL + companyFromRepo.company_id;
                                                                                                responseObj.jobtype = jobTypeFromRepo.type;
                                                                                                responseObj.jobtype_id = jobFromRepo.jobtype_id;
                                                                                                responseObj.jobtype_description = jobTypeFromRepo.description;
                                                                                                responseObj.post_date = jobPostsFromRepo.post_date;

                                                                                                responseArray.push(responseObj);
                                                                                                callback();
                                                                                            }

                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                            else {
                                                                                callback();
                                                                            }


                                                                        });
                                                                    },
                                                                    function (err) {
                                                                        if (err) {
                                                                            callback(err);
                                                                        } else {
                                                                            callback();

                                                                        }
                                                                    });
                                                            }
                                                            else {
                                                                callback('No Job Posts Found');
                                                            }
                                                        }
                                                    });
                                                }
                                                else {
                                                    callback('No User Found');
                                                }
                                            }
                                        });
                                    },
                                    function (err) {
                                        if (err) {
                                            res.statusCode = 404;
                                            res.send(err);
                                        } else {

                                            var jobsArray = {jobs: responseArray};
                                            res.json(jobsArray);

                                        }
                                    }
                                );
                            }
                            else {
                                res.statusCode = 404;
                                console.log('No Companies Found for your criteria.')
                                res.send("No Job Posts found for your criteria.");
                            }

                        });
                    }
                }
                else {
                    res.statusCode = 404;
                    res.send("No Seattings found");

                }
            }
        }
    );
}

// Get All Jobs
exports.getJobs = function (req, res) {

    var responseArray = [];
    var responseObj = {};

    Company.find({}, function (err, companiesList) {

        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {
            if (companiesList.length > 0) {

                // Testing

                async.forEach(companiesList, function (companyFromRepo, callback) {

                        User.findOne({company_id: companyFromRepo.company_id}, function (err, userFromRepo) {
                            if (err) {
                                callback(err);
                            }
                            else {
                                console.log(userFromRepo);
                                if (userFromRepo) {
                                    JobPost.find({poster_id: userFromRepo.username}, function (err, jobPostsFromRepoList) {
                                        if (err) {
                                            callback(err);
                                        }
                                        else {
                                            console.log('Job Post ' + jobPostsFromRepoList);
                                            if (jobPostsFromRepoList.length > 0) {

                                                async.forEach(jobPostsFromRepoList, function (jobPostsFromRepo, callback) {
                                                        Job.findOne({
                                                            job_id: jobPostsFromRepo.job_id
                                                        }, function (err, jobFromRepo) {
                                                            if (err) {
                                                                callback(err);
                                                            }
                                                            else if (jobFromRepo) {

                                                                JobType.findOne({jobtype_id: jobFromRepo.jobtype_id}).sort('description').exec(function (err, jobTypeFromRepo) {
                                                                    if (err) {
                                                                        callback(err);
                                                                    }
                                                                    else {
                                                                        responseObj = {};
                                                                        responseObj.jobId = jobFromRepo.job_id;
                                                                        responseObj.jobTitle = jobFromRepo.title;
                                                                        responseObj.company_logo = app_config.companyLogoBaseURL + companyFromRepo.company_id;
                                                                        responseObj.jobtype = jobTypeFromRepo.type;
                                                                        responseObj.jobtype_id = jobFromRepo.jobtype_id;
                                                                        responseObj.jobtype_description = jobTypeFromRepo.description;
                                                                        responseObj.post_date = jobPostsFromRepo.post_date;

                                                                        responseArray.push(responseObj);
                                                                        callback();
                                                                    }
                                                                });
                                                            }
                                                            else {
                                                                callback('No Job Found');
                                                            }

                                                        });
                                                    },
                                                    function (err) {
                                                        if (err) {
                                                            callback(err);
                                                        } else {
                                                            callback();
                                                        }
                                                    });
                                            }
                                            else {
                                                callback();
                                            }
                                        }
                                    });
                                }
                                else {
                                    callback('No User Found');
                                }
                            }
                        });
                    },
                    function (err) {
                        if (err) {
                            res.statusCode = 500;
                            res.send(err);
                        } else {

                            var jobsArray = {jobs: responseArray};
                            console.log(jobsArray);
                            res.json(jobsArray);

                        }
                    }
                );
            }
            else {
                res.statusCode = 404;
                console.log('No Companies Found for your criteria.')
                res.send("No Job Posts found for your criteria.");
            }
        }
    });

}

exports.getJobSearch = function (req, res) {

    JobSearch.findOne({jobsearchid: req.params.jobsearchid}, function (err, jobSearchFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.send(err);
        }
        else {
            res.send(jobSearchFromRepo);
        }
    });
}

exports.postJobSearch = function (req, res) {

    var newJobSearch = new JobSearch(req.body);
    var responseObject = {};
    responseObject.status = {};
    responseObject.message = {};
    JobType.findOne({jobtype_id: req.body.jobtype_id}, function (err, jobTypeFromRepo) {

        if (jobTypeFromRepo.type = 'A') {
            req.body.jobtype_id = null;
        }

        if (req.body.jobsearchid == null) {

            newJobSearch.jobsearchid = random.randomGUID();

            newJobSearch.save(function (err) {
                if (err) {
                    res.statusCode = 500;
                    responseObject.status = {0: 'Error in operation'};
                    responseObject.message = {0: err};
                    res.json(responseObject);
                }
                else {
                    responseObject.status = {1: 'Successful'};
                    responseObject.message = {jobsearchid: newJobSearch.jobsearchid};
                    res.json(responseObject);
                }
            });
        }
        else {
            JobSearch.findOne({jobsearchid: req.body.jobsearchid}, function (err, jobSearchFromRepo) {
                if (err) {
                    res.statusCode = 500;
                    responseObject.status = {0: 'Error in operation'};
                    responseObject.message = {0: err};
                    res.json(responseObject);
                }
                else {
                    if (jobSearchFromRepo) {
                        if (req.body.country_id != null || req.body.country_id != "") {
                            jobSearchFromRepo.country_id = req.body.country_id;
                        }
                        else if (req.body.region_id != null || req.body.region_id != "") {
                            jobSearchFromRepo.region_id = req.body.region_id;
                        }
                        else if (req.body.city_id != null || req.body.city_id != "") {
                            jobSearchFromRepo.city_id = req.body.city_id;
                        }
                        else if (req.body.jobsector_id != null || req.body.jobsector_id != "") {
                            jobSearchFromRepo.jobsector_id = req.body.jobsector_id;
                        }
                        else if (req.body.jobtype_id != null || req.body.jobtype_id != "") {
                            jobSearchFromRepo.jobtype_id = req.body.jobtype_id;
                        }
                        else if (req.body.date_range_start != null || req.body.date_range_start != "") {
                            jobSearchFromRepo.date_range_start = req.body.date_range_start;
                        }
                        else if (req.body.date_range_end != null || req.body.date_range_end != "") {
                            jobSearchFromRepo.date_range_end = req.body.date_range_end;
                        }
                        else if (req.body.postdate_id != null || req.body.postdate_id != "") {
                            jobSearchFromRepo.postdate_id = req.body.postdate_id;
                        }

                        jobSearchFromRepo.save(function (err) {
                            if (err) {
                                res.statusCode = 500;
                                res.send(err);
                            }
                            else {
                                res.statusCode = 200;
                                responseObject.status = {1: 'Successful'};
                                responseObject.message = {1: "Record Updated Successful"};
                                console.log(responseObject);
                                res.json(responseObject);
                            }
                        });
                    }
                    else {

                        res.statusCode = 404;
                        responseObject.status = {0: 'Error in operation'};
                        responseObject.message = {0: "No Settings found"};
                        res.json(responseObject);
                    }

                }
            });
        }
    });
}

exports.putJobSearch = function (req, res) {

    var editJobSearch = new JobSearch(req.body);

    JobSearch.findOne({jobsearchid: req.params.jobsearchid}, function (err, jobSearchFromRepo) {
        if (err) {
            res.statusCode = 500;
            responseObject.status = {0: 'Error in operation'};
            responseObject.message = {0: err};
            res.json(responseObject);
        }
        else {
            if (req.body.country_id != null || req.body.country_id != "") {
                jobSearchFromRepo.country_id = editJobSearch.country_id;
            }
            else if (req.body.region_id != null || req.body.region_id != "") {
                jobSearchFromRepo.region_id = editJobSearch.region_id;
            }
            else if (req.body.city_id != null || req.body.city_id != "") {
                jobSearchFromRepo.city_id = editJobSearch.city_id;
            }
            else if (req.body.jobsector_id != null || req.body.jobsector_id != "") {
                jobSearchFromRepo.jobsector_id = editJobSearch.jobsector_id;
            }
            else if (req.body.jobtype_id != null || req.body.jobtype_id != "") {
                jobSearchFromRepo.jobtype_id = editJobSearch.jobtype_id;
            }
            else if (req.body.date_range_start != null || req.body.date_range_start != "") {
                jobSearchFromRepo.date_range_start = editJobSearch.date_range_start;
            }
            else if (req.body.date_range_end != null || req.body.date_range_end != "") {
                jobSearchFromRepo.date_range_end = editJobSearch.date_range_end;
            }

            jobSearchFromRepo.save(function (err) {
                if (err) {
                    res.statusCode = 500;
                    responseObject.status = {0: 'Error in operation'};
                    responseObject.message = {0: err};
                    res.json(responseObject);
                }
                else {
                    res.statusCode = 200;
                    res.send("Record Updated Successful");
                }
            });
        }
    });

}


// Apply Job with CV URL
exports.postJobApplied = function (req, res) {
    var responseObject = {};
    responseObject.status = {};
    responseObject.message = {};

    var jobApplyObj = new JobApplied(req.body);


    jobApplyObj.jobapplied_id = random.randomGUID();

    jobApplyObj.save(function (err) {
        if (err) {
            res.statusCode = 500;
            responseObject.status = {0: 'Error in operation'};
            responseObject.message = {0: err};
            res.json(responseObject);
        }
        else {
            responseObject.status = {1: 'Successful'};
            responseObject.message = {jobapplied_id: jobApplyObj.jobapplied_id};
            res.json(responseObject);
        }
    });
}

// Search Jobs on the Basis of Search Criteria for only Applied Jobs
exports.getAppliedJobs = function (req, res) {

    var responseArray = [];
    var responseObj = {};

    JobSearch.findOne({jobsearchid: req.params.jobsearchid}, function (err, jobSearchFromRepo) {
            if (err) {
                res.statusCode = 500;
                res.send(err);
            }
            else {
                if (jobSearchFromRepo) {
                    searchReq = {};

                    if (jobSearchFromRepo.country_id == null || jobSearchFromRepo.country_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter country id");
                    }
                    else if (jobSearchFromRepo.region_id == null || jobSearchFromRepo.region_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter region id");
                    }
                    else if (jobSearchFromRepo.jobsector_id == null || jobSearchFromRepo.jobsector_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter job sector id");
                    }
                    else if (jobSearchFromRepo.jobtype_id == null || jobSearchFromRepo.jobtype_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter job type id");
                    }
                    else {
                        if (jobSearchFromRepo.city_id == null || jobSearchFromRepo.city_id == "") {
                            searchReq = {
                                country_id: jobSearchFromRepo.country_id,
                                region_id: jobSearchFromRepo.region_id,
                                jobsector_id: jobSearchFromRepo.jobsector_id
                            }
                        }
                        else if (jobSearchFromRepo.city_id != null) {
                            searchReq = {
                                country_id: jobSearchFromRepo.country_id,
                                region_id: jobSearchFromRepo.region_id,
                                city_id: jobSearchFromRepo.city_id,
                                jobsector_id: jobSearchFromRepo.jobsector_id
                            }
                        }
                        console.log(searchReq);

                        Company.find(searchReq, function (err, companiesList) {
                            console.log(companiesList);
                            if (err) {
                                res.statusCode = 500;
                                res.send(err);
                            }
                            else {
                                if (companiesList.length > 0) {

                                    // Testing

                                    async.forEach(companiesList, function (companyFromRepo, callback) {

                                            User.findOne({company_id: companyFromRepo.company_id}, function (err, userFromRepo) {
                                                if (err) {
                                                    callback(err);
                                                }
                                                else if (userFromRepo) {
                                                    JobPost.find({poster_id: userFromRepo.username}, function (err, jobPostsFromRepoList) {
                                                        if (err) {
                                                            callback(err);
                                                        }
                                                        else {
                                                            if (jobPostsFromRepoList.length > 0) {

                                                                async.forEach(jobPostsFromRepoList, function (jobPostsFromRepo, callback) {
                                                                        Job.findOne({
                                                                            job_id: jobPostsFromRepo.job_id,
                                                                            jobtype_id: jobSearchFromRepo.jobtype_id
                                                                        }, function (err, jobFromRepo) {
                                                                            if (err) {
                                                                                callback(err);
                                                                            }
                                                                            else if (jobFromRepo) {

                                                                                JobType.findOne({jobtype_id: jobFromRepo.jobtype_id}).sort('description').exec(function (err, jobTypeFromRepo) {
                                                                                    if (err) {
                                                                                        callback(err);
                                                                                    }
                                                                                    else {

                                                                                        JobApplied.find({jobsearchid: req.params.jobsearchid}, function (err, jobsAppliedFromRepoList) {
                                                                                            if (err) {
                                                                                                callback(err);
                                                                                            }
                                                                                            else if (jobsAppliedFromRepoList.length > 0) {

                                                                                                for (var i = 0; i < jobsAppliedFromRepoList.length; i++) {
                                                                                                    if (jobsAppliedFromRepoList[i].job_id == jobFromRepo.job_id) {

                                                                                                        responseObj = {};
                                                                                                        responseObj.jobId = jobFromRepo.job_id;
                                                                                                        responseObj.jobTitle = jobFromRepo.title;
                                                                                                        responseObj.company_logo = app_config.companyLogoBaseURL + companyFromRepo.company_id;
                                                                                                        responseObj.jobtype = jobTypeFromRepo.type;
                                                                                                        responseObj.jobtype_id = jobFromRepo.jobtype_id;
                                                                                                        responseObj.jobtype_description = jobTypeFromRepo.description;
                                                                                                        responseObj.post_date = jobPostsFromRepo.post_date;

                                                                                                        responseArray.push(responseObj);
                                                                                                        callback();
                                                                                                    }
                                                                                                    else {
                                                                                                        callback();
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            else {
                                                                                                callback();
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                            else {
                                                                                callback();
                                                                            }
                                                                        });
                                                                    },
                                                                    function (err) {
                                                                        if (err) {
                                                                            callback(err);
                                                                        } else {
                                                                            callback();

                                                                        }
                                                                    });
                                                            }
                                                            else {
                                                                //callback('No Job Posts Found');
                                                                callback();
                                                            }
                                                        }
                                                    });
                                                }
                                                else {
                                                    callback('No User Found');
                                                }

                                            });
                                        },
                                        function (err) {
                                            if (err) {
                                                res.statusCode = 404;
                                                res.send(err);
                                            } else {

                                                var jobsArray = {jobs: responseArray};
                                                res.json(jobsArray);

                                            }
                                        }
                                    );
                                }

                                else {
                                    res.statusCode = 404;
                                    console.log('No Companies Found for your criteria.')
                                    res.send("No Job Posts found for your criteria.");
                                }
                            }
                        });
                    }
                }
                else {
                    res.statusCode = 404;
                    res.send("No Seattings found");
                }
            }
        }
    );

}

// Search Jobs on the Basis of Search Criteria for only Applied Jobs
exports.getViewedJobs = function (req, res) {

    var responseArray = [];
    var responseObj = {};

    JobSearch.findOne({jobsearchid: req.params.jobsearchid}, function (err, jobSearchFromRepo) {
            if (err) {
                res.statusCode = 500;
                res.send(err);
            }
            else {
                if (jobSearchFromRepo) {
                    searchReq = {};

                    if (jobSearchFromRepo.country_id == null || jobSearchFromRepo.country_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter country id");
                    }
                    else if (jobSearchFromRepo.region_id == null || jobSearchFromRepo.region_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter region id");
                    }
                    else if (jobSearchFromRepo.jobsector_id == null || jobSearchFromRepo.jobsector_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter job sector id");
                    }
                    else if (jobSearchFromRepo.jobtype_id == null || jobSearchFromRepo.jobtype_id == "") {
                        res.statusCode = 400;
                        res.send("Please enter job type id");
                    }
                    else {
                        if (jobSearchFromRepo.city_id == null || jobSearchFromRepo.city_id == "") {
                            searchReq = {
                                country_id: jobSearchFromRepo.country_id,
                                region_id: jobSearchFromRepo.region_id,
                                jobsector_id: jobSearchFromRepo.jobsector_id
                            }
                        }
                        else if (jobSearchFromRepo.city_id != null) {
                            searchReq = {
                                country_id: jobSearchFromRepo.country_id,
                                region_id: jobSearchFromRepo.region_id,
                                city_id: jobSearchFromRepo.city_id,
                                jobsector_id: jobSearchFromRepo.jobsector_id
                            }
                        }
                        console.log(searchReq);

                        Company.find(searchReq, function (err, companiesList) {
                            console.log(companiesList);
                            if (err) {
                                res.statusCode = 500;
                                res.send(err);
                            }
                            else {
                                if (companiesList.length > 0) {

                                    // Testing

                                    async.forEach(companiesList, function (companyFromRepo, callback) {

                                            User.findOne({company_id: companyFromRepo.company_id}, function (err, userFromRepo) {
                                                if (err) {
                                                    callback(err);
                                                }
                                                else if (userFromRepo) {
                                                    JobPost.find({poster_id: userFromRepo.username}, function (err, jobPostsFromRepoList) {
                                                        if (err) {
                                                            callback(err);
                                                        }
                                                        else if (jobPostsFromRepoList.length > 0) {

                                                            async.forEach(jobPostsFromRepoList, function (jobPostsFromRepo, callback) {
                                                                    Job.findOne({
                                                                        job_id: jobPostsFromRepo.job_id,
                                                                        jobtype_id: jobSearchFromRepo.jobtype_id
                                                                    }, function (err, jobFromRepo) {
                                                                        if (err) {
                                                                            callback(err);
                                                                        }
                                                                        else if (jobFromRepo) {

                                                                            JobType.findOne({jobtype_id: jobFromRepo.jobtype_id}).sort('description').exec(function (err, jobTypeFromRepo) {
                                                                                if (err) {
                                                                                    callback(err);
                                                                                }
                                                                                else {
                                                                                    JobViewed.find({jobsearchid: req.params.jobsearchid}, function (err, jobsViewedFromRepoList) {
                                                                                        if (err) {
                                                                                            callback(err);
                                                                                        }
                                                                                        else if (jobsViewedFromRepoList.length > 0) {


                                                                                            async.forEach(jobsViewedFromRepoList, function (jobsViewedFromRepo, callback) {
                                                                                                if (jobsViewedFromRepo.job_id == jobFromRepo.job_id) {

                                                                                                    responseObj = {};
                                                                                                    responseObj.jobId = jobFromRepo.job_id;
                                                                                                    responseObj.jobTitle = jobFromRepo.title;
                                                                                                    responseObj.company_logo = app_config.companyLogoBaseURL + companyFromRepo.company_id;
                                                                                                    responseObj.jobtype = jobTypeFromRepo.type;
                                                                                                    responseObj.jobtype_id = jobFromRepo.jobtype_id;
                                                                                                    responseObj.jobtype_description = jobTypeFromRepo.description;
                                                                                                    responseObj.post_date = jobPostsFromRepo.post_date;

                                                                                                    responseArray.push(responseObj);
                                                                                                    callback();
                                                                                                }
                                                                                                else {
                                                                                                    callback();
                                                                                                }
                                                                                            }, function (err) {
                                                                                                if (err) {
                                                                                                    callback(err);
                                                                                                }
                                                                                                else {
                                                                                                    callback();
                                                                                                }
                                                                                            });

                                                                                        }
                                                                                        else {
                                                                                            callback();
                                                                                        }

                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                        else {
                                                                            callback();
                                                                        }

                                                                    });
                                                                },
                                                                function (err) {
                                                                    if (err) {
                                                                        callback(err);
                                                                    } else {
                                                                        callback();

                                                                    }
                                                                });
                                                        }
                                                        else {
                                                            callback();
                                                        }
                                                    });
                                                }
                                                else {
                                                    callback('No User Found');
                                                }

                                            });
                                        },
                                        function (err) {
                                            if (err) {
                                                res.statusCode = 500;
                                                res.send(err);
                                            } else {

                                                var jobsArray = {jobs: responseArray};
                                                res.json(jobsArray);

                                            }
                                        }
                                    );
                                }

                                else {
                                    res.statusCode = 404;
                                    console.log('No Companies Found for your criteria.')
                                    res.send("No Job Posts found for your criteria.");
                                }
                            }
                        });
                    }
                }
                else {
                    res.statusCode = 404;
                    res.send("No Seattings found");
                }
            }
        }
    );

}