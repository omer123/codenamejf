var mongoose = require('mongoose')
    , JobBand = require('../models/JobBandModel')
    , Currency = require('../models/CurrencyModel')
    , random = require('../utilities/random')
    , async = require('async');


// Get All
exports.getAll = function (req, res) {
    JobBand.find().sort('jobbandname').exec(function (err, JobBandFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            res.json(JobBandFromRepo);
        }
    });
}