var mongoose = require('mongoose')
    , Ad = require('../models/AdModel')
    , random = require('../utilities/random')
    , async = require('async');
var images = require('../utilities/images');


// Get All
exports.getAll = function (req, res) {

    var responseObject = {};
    responseObject.status = {};
    responseObject.message = {};

    Ad.find().sort('title').exec(function (err, AdFromRepo) {
        if (err) {
            res.statusCode = 500;
            responseObject.status = {0:'Error in operation'};
            responseObject.message = {0: err};
            res.json(err);
//            res.end();
        }
        else {

            responseObject.status = {1:'Successful'};
            responseObject.message = {1: AdFromRepo};
            res.json(AdFromRepo);
        }
    });
}

