var mongoose = require('mongoose')
    , Company = require('../models/CompanyModel')
    , Currency = require('../models/CurrencyModel')
    , User = require('../models/UserModel')
    , Country = require('../models/CountryModel')
    , City = require('../models/CityModel')
    , Region = require('../models/RegionModel')
    , JobSector = require('../models/JobSectorModel')
    , passport = require('passport')
    , async = require('async')
    , random = require('../utilities/random')
    , app_config = require('../config/app_config');


// Get All
exports.getAll = function (req, res) {
    Company.find(function (err, companiesList) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        var companiesResponseList=[];
        for (var i = 0; i < companiesList.length; i++) {
            // Fill Company Entity
            if (companiesList[i].companytitle == 'Ltd') {
                companiesList[i].companytitle = 'Private company limited by shares';
            } else if (companiesList[i].companytitle == 'Llp') {
                companiesList[i].companytitle = 'Limited liability partnership';
            } else if (companiesList[i].companytitle == 'Llc') {
                companiesList[i].companytitle = 'Limited liability company';
            }else if (companiesList[i].companytitle == 'Plc') {
                companiesList[i].companytitle = 'Public limited company';
            }else if (companiesList[i].companytitle == 'S-Corp') {
                companiesList[i].companytitle = 'S corporation';
            }else if (companiesList[i].companytitle == 'C-Corp') {
                companiesList[i].companytitle = 'C corporation';
            }else{
                companiesList[i].companytitle = 'N/A';
            }

            companiesResponseList.push(companiesList[i]);
        }

        async.forEach(companiesResponseList, function (companiesResponse, callback) {

                async.parallel([
                        //GET Countries
                        function (callback) {
                            companiesResponse.logo_image = app_config.companyLogoBaseURL + companiesResponse.company_id;

                            Country.findOne({country_id: companiesResponse.country_id}, function (err, countryFromRepo) {

                                if (!err) {
                                    companiesResponse.country_id = countryFromRepo.country_id;
                                    companiesResponse.countryname = countryFromRepo.countryname;
                                    callback();
                                }
                                else {
                                    callback(err);
                                }
                            });
                        },
                        //GET Regions
                        function (callback) {
                            Region.findOne({region_id: companiesResponse.region_id}, function (err, regionFromRepo) {

                                if (!err) {
                                    companiesResponse.region_id = regionFromRepo.region_id;
                                    companiesResponse.regionname = regionFromRepo.regionname;
                                    callback();
                                }
                                else {
                                    callback(err);
                                }
                            });
                        },
                        //Get Cities
                        function (callback) {
                            City.findOne({city_id: companiesResponse.city_id}, function (err, cityFromRepo) {

                                if (!err) {
                                    companiesResponse.city_id = cityFromRepo.city_id;
                                    companiesResponse.cityname = cityFromRepo.cityname
                                    callback();
                                }
                                else {
                                    callback(err);
                                }
                            });
                        }

                    ],
                    function (err) {
                        if (err) callback(err);
                        callback();
                    }
                );

            },
            function (err) {
                if (err) {
                    res.statusCode = 500;
                    res.end();
                } else {
                    res.json(companiesResponseList);
                }

            });
    });
}