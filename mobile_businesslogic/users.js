var mongoose = require('mongoose')
    , Company = require('../models/CompanyModel')
    , Currency = require('../models/CurrencyModel')
    , User = require('../models/UserModel')
    , Country = require('../models/CountryModel')
    , City = require('../models/CityModel')
    , Region = require('../models/RegionModel')
    , JobSector = require('../models/JobSectorModel')
    , passport = require('passport')
    , async = require('async')
    , random = require('../utilities/random');


// Get All
exports.getAll = function (req, res) {
    User.find().sort('username').exec(function (err, UsersFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            res.json(UsersFromRepo);
        }
    });
}