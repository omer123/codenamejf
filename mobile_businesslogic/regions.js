var mongoose = require('mongoose')
    , Region = require('../models/RegionModel')
    , Currency = require('../models/CurrencyModel')
    , random = require('../utilities/random')
    , async = require('async');


// Get All
exports.getAll = function (req, res) {
    Region.find().sort('regionname').exec(function (err, RegionFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            res.json(RegionFromRepo);
        }
    });
}

exports.getAllByCountryId = function (req, res) {
    Region.find({country_id: req.params.country_id}).sort('regionname').exec(function (err, RegionFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            res.json(RegionFromRepo);
        }
    });
}