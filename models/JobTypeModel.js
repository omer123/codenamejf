/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobTypeSchema = new mongoose.Schema(
    {
        jobtype_id	:	{type: String, unique: true, required: true},
        type	:	String, // (P/C/A/T/I)
        description	:	String // Permanent/ contract/ All/ Internship/ Temp
    }
);

//Create and export the model based
module.exports = mongoose.model('JobType', JobTypeSchema,"JobTypes");
