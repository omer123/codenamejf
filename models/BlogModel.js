/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var BlogSchema = new mongoose.Schema(
    {
        blog_id      : {type:String, unique:true, required:true},
        title	:	String,
        description	:	String
    }
);

//Create and export the model based
module.exports = mongoose.model('Blog', BlogSchema,"Blogs");
