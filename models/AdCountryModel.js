/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var AdCountrySchema = new mongoose.Schema(
    {
        ad_country_id      : {type:String, unique:true, required:true},
        ad_id	:	String,
        country_id	:	String
    }
);

//Create and export the model based
module.exports = mongoose.model('AdCountry', AdCountrySchema,"AdCountries");
