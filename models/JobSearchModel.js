/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobSearchSchema = new mongoose.Schema(
        {
            jobsearchid: {type: String, unique: true, required: true},
            country_id: String,
            region_id: String,  
            city_id: String,  
            jobsector_id: String,  
            jobtype_id: String,
            date_range_start: Date,
            date_range_end: Date,
            postdate_id: String,
            created : {type:Date, default:Date.now}

        }
    );

//Create and export the model based
module.exports = mongoose.model('JobSearch', JobSearchSchema, "JobSearchs");
