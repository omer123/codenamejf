/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobSectorSchema = new mongoose.Schema(
    {
        jobsector_id      : {type:String, unique:true, required:true},
        business_id	:	String,
        businesssector: String,
        jobband_id	:	String
    }
);

//Create and export the model based
module.exports = mongoose.model('JobSector', JobSectorSchema,"JobSector");
