/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var TechnicalSkillSchema = new mongoose.Schema(
    {
        skill_id      : {type:String, unique:true, required:true},
        skillname	:	String
    }
);

//Create and export the model based
module.exports = mongoose.model('TechnicalSkill', TechnicalSkillSchema,"TechnicalSkills");
