/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobBandPriceSchema = new mongoose.Schema(
    {
        jobbandprice_id      : {type:String, unique:true, required:true},
        jobband_id : String,
        price	:	Number
    }
);

//Create and export the model based
module.exports = mongoose.model('JobBandPrice', JobBandPriceSchema,"JobBandPrices");
