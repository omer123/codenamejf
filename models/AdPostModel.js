/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var AdPostSchema = new mongoose.Schema(
    {
        ad_post_id: {type: String, unique: true, required: true},
        ad_id: String,
        poster_id: String,
        post_date: {type: Date, default: Date.now()}
    }
);

//Create and export the model based
module.exports = mongoose.model('AdPost', AdPostSchema, "AdPosts");
