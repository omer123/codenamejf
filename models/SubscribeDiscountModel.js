/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var SubscriberDiscountSchema = new mongoose.Schema(
    {
        subscribe_discount_id      : {type:String, unique:true, required:true},
        subscribe_type_id	:	String,
        discount	:	Number
    }
);

//Create and export the model based
module.exports = mongoose.model('SubscriberDiscount', SubscriberDiscountSchema,"SubscriberDiscount");
