/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var ContactUsSchema = new mongoose.Schema(
    {
        contact_id      : {type:String, unique:true, required:true},
        name :	String,
        email: String,
        subject: String,
        message: String,
        country_id: String,
        created: {type:Date , default: Date.now}

    }
);

//Create and export the model based
module.exports = mongoose.model('ContactUs', ContactUsSchema,"Queries");