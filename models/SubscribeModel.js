/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var SubscribeSchema = new mongoose.Schema(
    {
        subscribe_type_id      : {type:String, unique:true, required:true},
        type	:	String // OneJob(0)/6months(1)/1year(2)
    }
);

//Create and export the model based
module.exports = mongoose.model('Subscribe', SubscribeSchema,"Subscribers");
