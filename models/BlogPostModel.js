/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var BlogPostSchema = new mongoose.Schema(
    {
        blog_post_id      : {type:String, unique:true, required:true},
        blog_id	:	String,
        poster_id	:	String,
        post_date : {type: Date, default: Date.now()}
    }
);

//Create and export the model based
module.exports = mongoose.model('BlogPost', BlogPostSchema,"BlogPosts");
