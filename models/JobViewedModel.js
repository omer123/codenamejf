/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobViewedSchema = new mongoose.Schema(
    {
        jobviewed_id      : {type:String, unique:true, required:true},
        job_id : String, 
        // USERID / SYSTEMGENERATED ID / JOBSEARCH ID same thing
        jobsearchid: String,
        created : {type: Date, default: Date.now()}
    }
);

//Create and export the model based
module.exports = mongoose.model('JobViewed', JobViewedSchema,"JobVieweds");