/**
 * Created by Rizwan on 2/3/14.
 */

//1. Import mongoose
var mongoose = require('mongoose');


//2. Create Country Schema using the above created mongoose object
var CountrySchema = new mongoose.Schema(

    {
        country_id	:	{type: String, unique: true, required: true},
        countryname	:	String,
        flag	:	String, // Flag Image Path
        currency_id	:	String,
        countrycode: String
    }
);

//3. Create Country Model based on Country Schema,Export the model so that, controller can use it
module.exports = mongoose.model('Country', CountrySchema, 'Countries');




//<!--<%  for(var i=0; i<countries.length; i++) {%>-->
//<!--<option value="<%countries[i].country_id%>"><%= countries[i].countryname %></option>-->
//<!--<% } %>-->
