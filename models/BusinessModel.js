/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var BusinessSchema = new mongoose.Schema(
    {
        business_id      : {type:String, unique:true, required:true},
        businessname	:	String
    }
);

//Create and export the model based
module.exports = mongoose.model('Business', BusinessSchema,"Businesses");
