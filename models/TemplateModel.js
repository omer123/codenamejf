/**
 * Created by suhail on 2/3/14.
 */

//1. Import mongoose
var mongoose = require('mongoose');


//2. Create Template Schema using the above created mongoose object
var TemplateSchema = new mongoose.Schema(
    {
        city_id: {type: String, unique: true},
        city_name: String,
        region: String,
        country:String,
        created: {type: Date, default: Date.now()}
    }
);

//3. Create Template Model based on Template Schema,Export the model so that, controller can use it.
module.exports = mongoose.model('Template', TemplateSchema, 'Templates');