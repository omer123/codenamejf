/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobPostSchema = new mongoose.Schema(
    {
        jobpost_id	:	{type: String, unique: true, required: true},
        job_id	    :	String,
        duration	:	String, // 1/2/3 (week or weeks)
        poster_id	:	String, // userid {Get Company ID from here}
        start_date	:	{type:Date, default: Date.now()},
        end_date	:	{type:Date, default: Date.now()},
        post_date   :   {type:Date, default: Date.now()}
    }
);

//Create and export the model based
module.exports = mongoose.model('JobPost', JobPostSchema,"JobPosts");
