/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var CurrencyCodeSchema = new mongoose.Schema(
    {
        user_subscribe_id: {type: String, unique: true, required: true},
        user_id: String,
        subscribe_id: String,
        subscription_start: {type: Date, default: Date.now()},
        subscription_end: {type: Date, default: Date.now()}
    }
);

//Create and export the model based
module.exports = mongoose.model('Currency', CurrencyCodeSchema, "Currencies");
