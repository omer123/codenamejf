/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var ToolSchema = new mongoose.Schema(
    {
        tool_id      : {type:String, unique:true, required:true},
        toolname	:	String
    }
);

//Create and export the model based
module.exports = mongoose.model('Tool', ToolSchema,"Tools");
