/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var CurrencyCodeSchema = new mongoose.Schema(
    {
        currency_id      : {type:String, unique:true, required:true},
        currencycode : String,
        currencyname	:	String,
        sign	:	String
    }
);

//Create and export the model based
module.exports = mongoose.model('Currency', CurrencyCodeSchema,"Currencies");
