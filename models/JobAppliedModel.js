/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobAppliedSchema = new mongoose.Schema(
    {
        jobapplied_id      : {type:String, unique:true, required:true},
        job_id : String,
        device_id : String,
        email : String,
        // USERID / SYSTEMGENERATED ID / JOBSEARCH ID same thing
        jobsearchid: String,
        cv_url: String,
        created : {type: Date, default: Date.now()}
    }
);

//Create and export the model based
module.exports = mongoose.model('JobApplied', JobAppliedSchema,"JobApplieds");