/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobAdLimitSchema = new mongoose.Schema(
    {
        job_ad_id      : {type:String, unique:true, required:true},
        subscribe_type_id	:	String,
        ad_limit	:	Number
    }
);

//Create and export the model based
module.exports = mongoose.model('JobAdLimit', JobAdLimitSchema,"JobAdLimits");
