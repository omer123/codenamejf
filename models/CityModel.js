/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var CitySchema = new mongoose.Schema(
    {
        city_id      : {type:String, unique:true, required:true},
        cityname	: String, // Settlement/City/Lga/ District Name
        type	: String, // Settlement(0)/City(1)/Lga(2)/District(3)
        region_id	: String
    }
);

//Create and export the model based
module.exports = mongoose.model('City', CitySchema,"Cities");
