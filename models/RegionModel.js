

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var RegionSchema = new mongoose.Schema(
    {
        region_id	:	{type: String, unique: true, required: true},
        regionname	:	String, // Region/State/Province Name
        type	:	String, // Region(0)/State(1)/Province(2)/District(3)
        country_id	:	String
    }
);

//Create and export the model based
module.exports = mongoose.model('Region', RegionSchema,"Regions");
