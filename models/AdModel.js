/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var AdSchema = new mongoose.Schema(
        {
            ad_id: {type: String, unique: true, required: true},
            title: String,
            ad_image: String, // Banner(0)/Logo(1) image path
            ad_text: String, // Banner/ServiceAnnouncement text
            ad_type: String, // Banner(0)/Logo(1)/ServiceAnnouncement(2)
            place_type: String // App(0)/Web(1)
        }
    );

//Create and export the model based
module.exports = mongoose.model('Ad', AdSchema, "Ads");
