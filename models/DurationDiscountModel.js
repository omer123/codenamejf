/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var DurationDiscountSchema = new mongoose.Schema(
    {
        duration_discount_id      : {type:String, unique:true, required:true},
        duration	:	Number,  // 1/2/3 (week or weeks)
        discount	:	Number // Discount rate
    }
);

//Create and export the model based
module.exports = mongoose.model('DurationDiscount', DurationDiscountSchema,"DurationDiscounts");
