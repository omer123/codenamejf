/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobSchema = new mongoose.Schema(
    {
        job_id	:	{type: String, unique: true, required: true},
        title	:	String,
        description	:	String,
        jobtype_id	:	String, //Job type(P/C/A/T/I)
        jobtype : String,

        location	:	String,

        company : String,


        // Search Criteria
        // Country, Region, City, Job SEctor, Job Type, Date Advertised

        //

        // NOT INTEGRATED YET
        //******************
        country_id: String,
        region_id: String,
        city_id: String,
        jobsector_id: String,
        //******************

        email	:	String,
        salary : String,
        salary_type	:	String, // Month(0)/Year(1)
        salary_low	:	String,
        salary_high	:	String,
        salary_currency_id	:	String,
        skills : [{
            skill_id :  String,
            skillname: String,
            required : String
        }],
        tools : [{
            tool_id :  String,
            toolname: String,
            required : String
        }],
        education : [{
            education :  String,
            required : String
        }],
        certification : [String], 
        experience	:	String
    }
);

//Create and export the model based
module.exports = mongoose.model('Job', JobSchema,"Jobs");
