/**
 * Created by Rizwan on 2/6/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var JobBandSchema = new mongoose.Schema(
    {
        jobband_id      : {type:String, unique:true, required:true},
        jobbandname	:	String // L/M/H/HH
    }
);

//Create and export the model based
module.exports = mongoose.model('JobBand', JobBandSchema,"JobBands");