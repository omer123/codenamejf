/**
 * Created by Rizwan on 2/4/14.
 */
/**
 * Created by SIGMA on 2/3/14.
 */

//Require Mongoose
var mongoose = require('mongoose');

//Define Schema
var CompanySchema = new mongoose.Schema(
    {
        company_id	:	{type: String, unique: true, required : true},
        companyname	:	{type: String, unique: true, required : true},
        companytype:	String,
        jobsector_id: String,
        companytitle: String,
        logo_image	:	String, // Company Logo Image path
        staff_range	:	String, // Staff Range(0: 1~20, 1:21~50, 2:51~)
        createdAt: { type: Date, default: Date.now },
        phone1: {type: String},
        phone2: {type: String},
        mobile1: {type: String},
        mobile2: {type: String},
        address1: {type: String},
        address2: {type: String},
        address3: {type: String},
        city_id: { type: String, required: false, index: { unique: false } },
        cityname: String,
        region_id: { type: String, required: false, index: { unique: false } },
        regionname: String,
        country_id: { type: String, required: false, index: { unique: false } },
        countryname: String,
        website: { type: String, required: false, index: { unique: false } },
        active: String
    }
);

//Create and export the model based
module.exports = mongoose.model('Company', CompanySchema, 'Companies');

