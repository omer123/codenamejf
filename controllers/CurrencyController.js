/**
 * Created by SIGMA on 2/5/14.
 */

var currencies = require('./../businesslogic/currencies')

module.exports.controller = function (app) {

    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) return next();
        req.flash('error', 'Please sign in to continue.');
        var postAuthDestination = req.url;
        res.redirect('/activejobfinder/web/users/login?postAuthDestination=' + postAuthDestination);
    }

    app.get('/activejobfinder/web/currencies/register', ensureAuthenticated, currencies.register);
    app.post('/activejobfinder/web/currencies/register',  ensureAuthenticated, currencies.currencyValidations, currencies.create);
    app.get('/activejobfinder/web/currencies/account', ensureAuthenticated, currencies.account);
    app.post('/activejobfinder/web/currencies/account',  ensureAuthenticated,  currencies.currencyValidations, currencies.update);
    app.get('/activejobfinder/web/currencies/dashboard',   currencies.dashboard);
    app.get('/activejobfinder/web/currencies', ensureAuthenticated,  currencies.list); // for illustrative purposes only



}