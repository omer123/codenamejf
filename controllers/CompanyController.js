/**
 * Created by SIGMA on 2/5/14.
 */

var companies = require('./../businesslogic/companies')
var companiesForMobile = require('./../mobile_businesslogic/companies')

module.exports.controller = function (app) {

    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) return next();
        req.flash('error', 'Please sign in to continue.');
        var postAuthDestination = req.url;
        res.redirect('/activejobfinder/web/users/login?postAuthDestination=' + postAuthDestination);
    }

    app.get('/register', companies.register);
    app.post('/activejobfinder/web/companies/register',  companies.companyValidations, companies.create);
    app.get('/activejobfinder/web/companies/account', companies.account);
    app.post('/activejobfinder/web/companies/account',   companies.companyValidations, companies.update);
    app.get('/activejobfinder/web/companies/dashboard',   companies.dashboard);
    app.get('/activejobfinder/web/companies',  ensureAuthenticated, companies.list); // for illustrative purposes only


    app.get('/activejobfinder/companies', companiesForMobile.getAll)

}