/**
 * Created by SIGMA on 2/5/14.
 */

var countries = require('./../businesslogic/countries');
var countriesForMobile = require('./../mobile_businesslogic/countries');
var Region = require('../models/RegionModel')
    , Country = require('../models/CountryModel');
var async = require('async');
var fs = require('fs');
var csv = require('csv');
var random = require('../utilities/random');


module.exports.controller = function (app) {

    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) return next();
        req.flash('error', 'Please sign in to continue.');
        var postAuthDestination = req.url;
        res.redirect('/activejobfinder/web/users/login?postAuthDestination=' + postAuthDestination);
    }

    app.get('/activejobfinder/web/countries/register', ensureAuthenticated, countries.register);
    app.post('/activejobfinder/web/countries/register', ensureAuthenticated, countries.countryValidations, countries.create);
    app.get('/activejobfinder/web/countries/account/:countryid', ensureAuthenticated, countries.account);
    app.post('/activejobfinder/web/countries/account', ensureAuthenticated, countries.countryValidations, countries.update);
    app.get('/activejobfinder/web/countries/dashboard', countries.dashboard);
    app.get('/activejobfinder/web/countries', ensureAuthenticated, countries.list); // for illustrative purposes only

    app.delete('/activejobfinder/web/countries/:countryid', countries.deleteCountry);


    // Mobile App Web Services
    app.get('/activejobfinder/countries', countriesForMobile.getAll);

    //  Upload Excel file
    app.post('/activejobfinder/web/countries/upload/file', function (req, res) {

        var records = [];
        var newCountry;
        var countriesForSave = [];

        csv()
            .from.stream(fs.createReadStream(req.files.fileUpload.path))
            .to.path(__dirname + '/sample.out')
            .transform(function (row) {
                row.unshift(row.pop());
                return row;
            })
            .on('record', function (row, index) {
                records.push(row);
            })
            .on('end', function (count) {
                console.log('Number of lines: ' + records.length);
                console.log(records);

                async.forEach(records, function (line, callback) {


                        var newEntry = {};
                        newEntry.country_id = random.randomGUID();

                        if (line[0] != null) {
                            newEntry.countryname = line[1].trim();
                        }
                        if (line[1] != null) {
                            newEntry.currency_id = line[0].trim();
                        }

                        Country.findOne({countryname: new RegExp('^' + newEntry.countryname + '$', "i")}, function (err, countryFromRepo) {

                            if (countryFromRepo) {
                                console.log('Country Name Already Exist');
                                callback();
                            }
                            else {

                                newCountry = new Country(newEntry);

                                newCountry.save(function (err) {
                                    if (err) {
                                        callback(err);
                                    } else {
                                        countriesForSave.push(newEntry.countryname);
                                        callback();
                                    }
                                });
                            }

                        });

                    },
                    // Final Callback of Async.ForEach
                    function (err) {
                        if (err) {
                            req.flash('error', err);
                            return res.redirect('/activejobfinder/web/countries');
                        }
                        else {
                            console.log(countriesForSave);
                            req.flash('success', 'Following Cities are added :\n'+ JSON.stringify(countriesForSave));
                            return res.redirect('/activejobfinder/web/countries');
                        }
                    }
                );
            }).on('error', function (error) {
                console.log(error.message);
            });
    });

}