/**
 * Created by SIGMA on 2/5/14.
 */

var users = require('./../businesslogic/users');
var dao = require('./../dao/HelperFunctions');

var Country = require('../models/CountryModel');
var usersForMobile = require('./../mobile_businesslogic/users');
var images = require('../utilities/images');

module.exports.controller = function (app) {


    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) return next();
        req.flash('error', 'Please sign in to continue.');
        var postAuthDestination = req.url;
        res.redirect('/login?postAuthDestination=' + postAuthDestination);
    }

    function redirectAuthenticated(req, res, next) {
        if (req.isAuthenticated()) return res.redirect('/');
        next();
    }

    app.get('/activejobfinder/web/users/login', redirectAuthenticated, users.login);
    app.get('/activejobfinder/web/company/login', redirectAuthenticated, users.companyLogin);


    app.get('/activejobfinder/web/users/reset_password', redirectAuthenticated, users.reset_password);
    app.post('/activejobfinder/web/users/reset_password', redirectAuthenticated, users.generate_password_reset);
    app.get('/activejobfinder/web/users/password_reset', redirectAuthenticated, users.password_reset);
    app.post('/activejobfinder/web/users/password_reset', redirectAuthenticated, users.process_password_reset);

    app.post('/activejobfinder/web/users/login', redirectAuthenticated, users.authenticate);
    app.post('/activejobfinder/web/company/login', redirectAuthenticated, users.authenticateCompany);

    app.get('/activejobfinder/web/users/register', redirectAuthenticated, users.register);

    app.get('/activejobfinder/web/users/new',  users.register);

    app.post('/activejobfinder/web/users/register', redirectAuthenticated, users.userValidations, users.create);
    app.get('/activejobfinder/web/users/account', ensureAuthenticated, users.account);
    app.post('/activejobfinder/web/users/account', ensureAuthenticated, users.userValidations, users.update);
    app.get('/activejobfinder/web/users/dashboard', ensureAuthenticated, users.dashboard);
    app.get('/activejobfinder/web/users/logout', users.logout);
    app.get('/activejobfinder/web/users', ensureAuthenticated, users.list); // for illustrative purposes only

    app.post('/activejobfinder/web/users/company/register',  users.createCompanyAndUser);
    app.post('/activejobfinder/web/users/company/account',  users.accountCompanyAndUser);

    // Get Image by Userid
    /// Show files
    app.get('/activejobfinder/web/company/images/:companyname', function (req, res) {

            images.companyImage(req, res);
        }
    );

    app.post('/activejobfinder/web/save/company/image', function (req, res) {
            console.log('got you');
            console.log(req.body.companyname.toLowerCase());

            images.saveCompanyImageWeb(req, res);
        }
    );

    // *************** MOBILE SERVICES ****************** //

    app.get('/activejobfinder/users', usersForMobile.getAll);

}