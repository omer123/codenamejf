/**
 * Created by SIGMA on 2/5/14.
 */

var cities = require('./../businesslogic/cities');
var citiesForMobile = require('./../mobile_businesslogic/cities');
var async = require('async');
var fs = require('fs');
var csv = require('csv');
var random = require('../utilities/random');
var City = require('../models/CityModel');


module.exports.controller = function (app) {

    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) return next();
        req.flash('error', 'Please sign in to continue.');
        var postAuthDestination = req.url;
        res.redirect('/activejobfinder/web/users/login?postAuthDestination=' + postAuthDestination);
    }

    app.get('/activejobfinder/web/cities/register', ensureAuthenticated, cities.register);
    app.post('/activejobfinder/web/cities/register', ensureAuthenticated, cities.cityValidations, cities.create);
    app.get('/activejobfinder/web/cities/account/:cityid', ensureAuthenticated, cities.account);
    app.post('/activejobfinder/web/cities/account', ensureAuthenticated, cities.cityValidations, cities.update);
    app.get('/activejobfinder/web/cities', ensureAuthenticated, cities.list); // for illustrative purposes only

    // JSONS RESPONSES
    app.get('/activejobfinder/web/cities/:regionid', cities.getCitiesByRegionId);
    app.delete('/activejobfinder/web/cities/:cityid', cities.deleteCity);


    // Mobile App Web Services
    app.get('/activejobfinder/cities', citiesForMobile.getAll);
    app.get('/activejobfinder/cities/:region_id', citiesForMobile.getAllByRegionId);

    //  Upload Excel file
    app.post('/activejobfinder/web/cities/upload/file', function (req, res) {

        var records = [];
        var newCity;
        var citiesForSave = [];

        csv()
            .from.stream(fs.createReadStream(req.files.fileUpload.path))
            .to.path(__dirname + '/sample.out')
            .transform(function (row) {
                row.unshift(row.pop());
                return row;
            })
            .on('record', function (row, index) {
                records.push(row);
            })
            .on('end', function (count) {
                console.log('Number of lines: ' + records.length);
                console.log(records);

                async.forEach(records, function (line, callback) {


                        var newEntry = {};
                        newEntry.city_id = random.randomGUID();

                        if (line[1] != null) {
                            newEntry.cityname = line[1].trim();
                        }
                        if (line[2] != null) {
                            newEntry.region_id = line[0].trim();
                        }
                        if (line[0] != null) {
                            newEntry.type = line[2].trim();
                        }

                        City.findOne({cityname: new RegExp('^' + newEntry.cityname + '$', "i")}, function (err, cityFromRepo) {

                            if (cityFromRepo) {
                                console.log('City Name Already Exist');
                                callback();
                            }
                            else {
                                newCity = new City(newEntry);

                                newCity.save(function (err) {
                                    if (err) {
                                        callback(err);
                                    } else {
                                        citiesForSave.push(newEntry.cityname);
                                        callback();
                                    }
                                });
                            }

                        });


                    },
                    // Final Callback of Async.ForEach
                    function (err) {
                        if (err) {
                            req.flash('error', err);
                            return res.redirect('/activejobfinder/web/cities');
                        }
                        else {
                            console.log(citiesForSave);
                            req.flash('success', 'Following Cities are added :\n'+ JSON.stringify(citiesForSave));
                            return res.redirect('/activejobfinder/web/cities');
                        }
                    }
                );
            }).on('error', function (error) {
                console.log(error.message);
            });
    });
}