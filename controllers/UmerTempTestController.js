/**
 * Created by SIGMA on 2/5/14.
 */


module.exports.controller = function (app) {

    app.get('/temptest/register', function (req, res) {
        res.render('temptest/index');
    });

    app.get('/temptest/jobdetails', function (req, res) {
        res.render('temptest/jobdetails');
    });

    app.get('/temptest/joblist', function (req, res) {
        res.render('temptest/joblist');
    });

    app.get('/temptest/jobpost', function (req, res) {
        res.render('temptest/jobpost');
    });

    app.get('/temptest/home', function (req, res) {
        res.render('temptest/home');
    });


}