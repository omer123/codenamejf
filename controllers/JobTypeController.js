/**
 * Created by SIGMA on 2/5/14.
 */

var jobtypes = require('./../businesslogic/jobtypes')
var jobtypesForMobile = require('./../mobile_businesslogic/jobtypes')

module.exports.controller = function (app) {

    app.get('/activejobfinder/web/jobtypes/register', jobtypes.register);
    app.post('/activejobfinder/web/jobtypes/register',  jobtypes.jobtypeValidations, jobtypes.create);
    app.get('/activejobfinder/web/jobtypes/account', jobtypes.account);
    app.post('/activejobfinder/web/jobtypes/account',   jobtypes.jobtypeValidations, jobtypes.update);
    app.get('/activejobfinder/web/jobtypes/dashboard',   jobtypes.dashboard);
    app.get('/activejobfinder/web/jobtypes',  jobtypes.list); // for illustrative purposes only


    // Mobile App Web Services
    app.get('/activejobfinder/jobtypes',  jobtypesForMobile.getAll);
}