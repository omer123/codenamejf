/**
 * Created by SIGMA on 2/5/14.
 */

var ads = require('./../businesslogic/ads');
var adsForMobile = require('./../mobile_businesslogic/ads');
var images = require('../utilities/images');

module.exports.controller = function (app) {

    // For Company Dashboard Login
    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) return next();
        req.flash('error', 'Please sign in to continue.');
        var postAuthDestination = req.url;
        res.redirect('/activejobfinder/web/users/login?postAuthDestination=' + postAuthDestination);
    }

    app.get('/activejobfinder/web/ads/register', ads.register);
//    app.post('/activejobfinder/web/ads/register',  ads.jobtypeValidations, ads.create);
    app.get('/activejobfinder/web/ads/account', ads.account);
//    app.post('/activejobfinder/web/ads/account',   ads.jobtypeValidations, ads.update);
    app.get('/activejobfinder/web/ads/dashboard',   ads.dashboard);
    app.get('/activejobfinder/web/ads',  ads.list); // for illustrative purposes only




    // Mobile App Web Services

    app.get('/activejobfinder/ads',  adsForMobile.getAll);

    app.get('/activejobfinder/web/ads/images/:adid', function (req, res) {

            images.adImage(req, res);
        }
    );
}