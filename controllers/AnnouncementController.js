/**
 * Created by SIGMA on 2/5/14.
 */

var mongoose = require('mongoose')
    , Ad = require('../models/AdModel')
    , random = require('../utilities/random')
    , async = require('async')
    , app_config = require('../config/app_config')
    , images = require('../utilities/images');

module.exports.controller = function (app) {

    app.get('/activejobfinder/announcement', function (req, res) {

        Ad.find({place_type: "0", ad_type: "2"}, function (err, announcementFromRepo) {

            if (err) {
                res.statusCode = 500;
                res.send(err);
            } else {
                if (announcementFromRepo.length > 0) {
                    var responseList = [];
                    for (var i = 0; i < announcementFromRepo.length; i++) {
                        announcementFromRepo[i].ad_image = app_config.announcmentBaseURL + announcementFromRepo[i].ad_id;

                        var obj = {};

                        obj.image_url = announcementFromRepo[i].ad_image;
                        obj.title = announcementFromRepo[i].title;
                        obj.description= announcementFromRepo[i].ad_text;

                        responseList.push(obj);
                    }
                    res.json(responseList);
                }
                else {
                    res.json([]);
                }
            }
        });

    });


}