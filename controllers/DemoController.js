/**
 * Created by Rizwan on 10/21/14.
 */

/**
 * Created by suhail on 2/9/14.
 */

// 1. Import Mongoose and model this controller will use

var mongoose = require('mongoose');
var template = require('../models/TemplateModel');
var random = require('../utilities/random');

//2. Define a function which configures the routes of the express app object passed from the app.js
module.exports.controller = function (app) {

    // GET All
    app.get('/api/cities/all', function (req, res) {

        template.find(function (err, templateFromRepo) {
            if (err) {
                res.setHeader('Response-Description', err);
                res.statusCode = 500;
                res.end();
            }
            else {
                res.json(templateFromRepo);
            }
        });
    });

    // GET By City User ID
    app.get('/api/cities/:id', function (req, res) {

        template.findOne({city_id: req.params.id}, function (err, templateFromRepo) {
            if (templateFromRepo) {
                if (!err) {
                    res.json(templateFromRepo);
                } else {
                    res.setHeader('Response-Description', err);
                    res.statusCode = 500;
                    res.end();
                }
            }
            else {
                res.setHeader('Response-Description', 'No City Found');
                res.statusCode = 500;
                res.end();
            }

        });
    });

    // GET By City User ID
    app.get('/api/cities/name/:id', function (req, res) {

        template.findOne({city_name: req.params.id}, function (err, templateFromRepo) {
            if (templateFromRepo) {
                if (!err) {
                    res.json(templateFromRepo);
                } else {
                    res.setHeader('Response-Description', err);
                    res.statusCode = 500;
                    res.end();
                }
            }
            else {
                res.setHeader('Response-Description', 'No City Found');
                res.statusCode = 500;
                res.end();
            }

        });
    });


    //Define POST endpoint or route
    app.post('/api/cities', function (req, res) {
        console.log(req.body);
        var templateObject = new template(req.body);
        //find conflict [Check Duplicate city_id]
        template.findOne({city_name: templateObject.city_name}, function (err, templateFromRepo) {
            if (templateFromRepo) {
                res.setHeader('Response-Description', 'City Already Exist');
                res.statusCode = 409;
                res.end();
            }
            else {
                templateObject.city_id = random.randomGUID();;
                console.log(templateObject);
                templateObject.save(function (err) {
                    if (!err) {
                        res.statusCode = 201;
                        res.end();
                    } else {
                        res.setHeader('Response-Description', err);
                        res.statusCode = 500;
                        res.end();
                    }
                });
            }
        });
    });

    //2.3 Define PUT endpoint or route by City User ID
    app.put('/api/cities/:city_id', function (req, res) {
        template.findOne({city_id: req.params.city_id}, function (err, templateFromRepo) {
            if (templateFromRepo) {

                if (req.body.city_name != null) {
                    templateFromRepo.city_name = req.body.city_name
                }
                if (req.body.region != null) {
                    templateFromRepo.region = req.body.region
                }
                if (req.body.country != null) {
                    templateFromRepo.country = req.body.country
                }

                if (!err) {
                    templateFromRepo.save(function (err) {
                        if (!err) {
                            res.statusCode = 200;
                            res.end();
                        }
                        else {
                            res.setHeader('Response-Description', err);
                            res.statusCode = 500;
                            res.end();
                        }
                    });
                } else {
                    res.setHeader('Response-Description', err);
                    res.statusCode = 500;
                    res.end();
                }
            }
            else {
                res.setHeader("Response-Description", "City Not Found");
                res.statusCode = 404;
                res.end();
            }
        });
    });

    // Delete By CATEGORY Id
    app.delete('/api/cities/:city_id', function (req, res) {
        template.remove({city_id: req.params.city_id}, function (err) {
            if (!err) {
                res.statusCode = 200;
                res.end();
            }
            else {
                res.setHeader('Response-Description', err);
                res.statusCode = 500;
                res.end();
            }
        });

    });
};
