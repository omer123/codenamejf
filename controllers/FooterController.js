/**
 * Created by SIGMA on 2/5/14.
 */

var footer = require('./../businesslogic/footer');

module.exports.controller = function (app) {

    // FOOTER SECTION
    app.get('/activejobfinder/web/privacypolicy', footer.privacypolicy);
    app.get('/activejobfinder/web/termsandconditions', footer.termsandconditions);
    app.get('/activejobfinder/web/aboutus', footer.aboutus);

    app.get('/activejobfinder/web/contactus', footer.contactus);
    app.get('/activejobfinder/web/queries', footer.queries);

    app.post('/activejobfinder/web/contactus', footer.contactusCreate);



}