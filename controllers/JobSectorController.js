/**
 * Created by SIGMA on 2/5/14.
 */

var jobsectors = require('./../businesslogic/jobsectors')
var jobsectorsForMobile = require('./../mobile_businesslogic/jobsectors')

module.exports.controller = function (app) {

    app.get('/activejobfinder/web/jobsectors/register', jobsectors.register);
    app.post('/activejobfinder/web/jobsectors/register',   jobsectors.create);
//    app.post('/activejobfinder/web/jobsectors/register',  jobsectors.jobsectorValidations, jobsectors.create);
    app.get('/activejobfinder/web/jobsectors/account', jobsectors.account);
    app.post('/activejobfinder/web/jobsectors/account',   jobsectors.jobsectorValidations, jobsectors.update);
    app.get('/activejobfinder/web/jobsectors/dashboard',   jobsectors.dashboard);
    app.get('/activejobfinder/web/jobsectors',  jobsectors.list); // for illustrative purposes only


    // Mobile App Web Services
    app.get('/activejobfinder/jobsectors',  jobsectorsForMobile.getAll);
}