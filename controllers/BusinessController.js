/**
 * Created by SIGMA on 2/5/14.
 */

var businesses = require('./../businesslogic/businesses')

module.exports.controller = function (app) {

    app.get('/activejobfinder/web/businesses/register', businesses.register);
    app.post('/activejobfinder/web/businesses/register',  businesses.businessValidations, businesses.create);
    app.get('/activejobfinder/web/businesses/account', businesses.account);
    app.post('/activejobfinder/web/businesses/account',   businesses.businessValidations, businesses.update);
    app.get('/activejobfinder/web/businesses/dashboard',   businesses.dashboard);
    app.get('/activejobfinder/web/businesses',  businesses.list); // for illustrative purposes only

}