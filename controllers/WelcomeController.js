/**
 * Created by SIGMA on 2/5/14.
 */

var welcome = require('./../businesslogic/welcome');
var images = require('../utilities/images');

module.exports.controller = function (app) {

    app.get('/cmslogin', welcome.index_login);
    //    app.get('/', welcome.index);
    //app.get('/', function(req, res){
    //    if(app.address().port == 8005){
    //        // temptest
    //        res.render('temptest/home');
    //    }
    //    else{
    //        res.render('welcome/underconstruction');
    //    }
    //});

    app.get('/underconstruction', welcome.underconstruction);

    app.get('/activejobfinder/web/company/images/:companyname', function (req, res) {

            images.companyImage(req, res);
        }
    );

    app.all('*', welcome.not_found);

}