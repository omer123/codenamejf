/**
 * Created by SIGMA on 2/5/14.
 */

var regions = require('./../businesslogic/regions');
var regionsForMobile = require('./../mobile_businesslogic/regions');
var Region = require('../models/RegionModel');
var async = require('async');
var fs = require('fs');
var csv = require('csv');
var random = require('../utilities/random');


module.exports.controller = function (app) {


    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) return next();
        req.flash('error', 'Please sign in to continue.');
        var postAuthDestination = req.url;
        res.redirect('/activejobfinder/web/users/login?postAuthDestination=' + postAuthDestination);
    }


    app.get('/activejobfinder/web/regions/register', ensureAuthenticated, regions.register);
    app.post('/activejobfinder/web/regions/register', ensureAuthenticated, regions.regionValidations, regions.create);
    app.get('/activejobfinder/web/regions/account/:regionid', ensureAuthenticated, regions.account);
    app.post('/activejobfinder/web/regions/account', ensureAuthenticated, regions.regionValidations, regions.update);
    app.get('/activejobfinder/web/regions/dashboard', regions.dashboard);
    app.get('/activejobfinder/web/regions', ensureAuthenticated, regions.list); // for illustrative purposes only

    // JSONS RESPONSES
    app.get('/activejobfinder/web/regions/:countryid', regions.getRegionsByCountryId);
    app.delete('/activejobfinder/web/regions/:regionid', regions.deleteRegion);

    // Mobile App Web Services
    app.get('/activejobfinder/regions', regionsForMobile.getAll);
    app.get('/activejobfinder/regions/:country_id', regionsForMobile.getAllByCountryId);

    //  Upload Excel file
    app.post('/activejobfinder/web/regions/upload/file', function (req, res) {

        var records = [];
        var newRegion;
        var regionsForSave = [];

        csv()
            .from.stream(fs.createReadStream(req.files.fileUpload.path))
            .to.path(__dirname + '/sample.out')
            .transform(function (row) {
                row.unshift(row.pop());
                return row;
            })
            .on('record', function (row, index) {
                records.push(row);
            })
            .on('end', function (count) {
                console.log('Number of lines: ' + records.length);
                console.log(records);

                async.forEach(records, function (line, callback) {


                        var newEntry = {};
                        newEntry.region_id = random.randomGUID();

                        if (line[1] != null) {
                            newEntry.regionname = line[1].trim();
                        }
                        if (line[2] != null) {
                            newEntry.country_id = line[0].trim();
                        }
                        if (line[0] != null) {
                            newEntry.type = line[2].trim();
                        }

                        Region.findOne({regionname: new RegExp('^' + newEntry.regionname + '$', "i")}, function (err, regionFromRepo) {

                            if (regionFromRepo) {
                                console.log('Region Name Already Exist');
                                callback();
                            }
                            else {

                                newRegion = new Region(newEntry);

                                newRegion.save(function (err) {
                                    if (err) {
                                        callback(err);
                                    } else {
                                        regionsForSave.push(newEntry.regionname);
                                        callback();
                                    }
                                });

                            }

                        });


                    },
                    // Final Callback of Async.ForEach
                    function (err) {
                        if (err) {
                            req.flash('error', err);
                            return res.redirect('/activejobfinder/web/regions');
                        }
                        else {
                            console.log(regionsForSave);
//                            req.flash('success', JSON.stringify(regionsForSave));
                            return res.redirect('/activejobfinder/web/regions');
                        }
                    }
                );
            }).on('error', function (error) {
                console.log(error.message);
            });
    });
}