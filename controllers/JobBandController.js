/**
 * Created by SIGMA on 2/5/14.
 */

var jobbands = require('./../businesslogic/jobbands')
var jobbandsForMobile = require('./../mobile_businesslogic/jobbands')

module.exports.controller = function (app) {

    app.get('/activejobfinder/web/jobbands/register', jobbands.register);
    app.post('/activejobfinder/web/jobbands/register',  jobbands.jobbandValidations, jobbands.create);
    app.get('/activejobfinder/web/jobbands/account', jobbands.account);
    app.post('/activejobfinder/web/jobbands/account',   jobbands.jobbandValidations, jobbands.update);
    app.get('/activejobfinder/web/jobbands/dashboard',   jobbands.dashboard);
    app.get('/activejobfinder/web/jobbands',  jobbands.list); // for illustrative purposes only


    // Mobile App Web Services
    app.get('/activejobfinder/jobbands',  jobbandsForMobile.getAll);

}