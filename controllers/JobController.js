/**
 * Created by SIGMA on 2/5/14.
 */

var jobs = require('./../businesslogic/jobs');
var jobsForMobile = require('./../mobile_businesslogic/jobs');
var cv = require('../utilities/cv');

module.exports.controller = function (app) {

    app.get('/activejobfinder/web/jobs/register', jobs.register);
    app.post('/activejobfinder/web/jobs/register',  jobs.jobValidations, jobs.create);
    app.get('/activejobfinder/web/jobs/account', jobs.account);
    app.post('/activejobfinder/web/jobs/account',   jobs.jobValidations, jobs.update);
    app.get('/activejobfinder/web/jobs/dashboard',   jobs.dashboard);
    app.get('/activejobfinder/web/jobs',  jobs.list); // for illustrative purposes only


    // Mobile App Web Services

    //app.get('/activejobfinder/jobs',  jobsForMobile.getAll);
    app.get('/activejobfinder/jobdetails/:jobsearchid/:job_id',  jobsForMobile.getById);

    //-- JOB POSTS
    app.get('/activejobfinder/jobposts',  jobsForMobile.getAllJobPosts);
    app.get('/activejobfinder/jobpostsandjobdetails/:company_id',  jobsForMobile.getJobPostsByCompany);
    app.get('/activejobfinder/jobpostsandjobdetails',  jobsForMobile.getAllJobPostsAndJobDetail);

    //-- JOB SEARCH
    // Request Criteria
    // country_id, region_id, city_id, jobsector_id, jobtype_id, Date Range

    app.get('/activejobfinder/jobssearch/:jobsearchid', jobsForMobile.getJobSearch);
    app.get('/activejobfinder/jobs/notapplied/:jobsearchid', jobsForMobile.getJobsFromJobSearch);
    app.get('/activejobfinder/jobs', jobsForMobile.getJobs );
    app.post('/activejobfinder/jobssearch', jobsForMobile.postJobSearch);
    app.put('/activejobfinder/jobssearch/:jobsearchid', jobsForMobile.putJobSearch);


    //-- JOB SKILLS & TOOLS
    app.get('/activejobfinder/jobsskills',  jobsForMobile.getAllJobTechnicalSkills);
    app.get('/activejobfinder/jobstools',  jobsForMobile.getAllJobTools);

    app.get('/activejobfinder/jobsskills/:id',  jobsForMobile.getAllJobTechnicalSkillById);
    app.get('/activejobfinder/jobstools/:id',  jobsForMobile.getAllJobToolById);


    // Applied Jobs
    app.post('/activejobfinder/jobsapplied', jobsForMobile.postJobApplied);
    app.get('/activejobfinder/jobsapplied/:jobsearchid', jobsForMobile.getAppliedJobs);


    // Viewed Jobs
    app.get('/activejobfinder/jobsviewed/:jobsearchid', jobsForMobile.getViewedJobs);

    // Upload CV
    app.post('/activejobfinder/jobdetails/cv/upload', function(req,res){
        cv.uploadCV(req,res);
    });


    app.get('/activejobfinder/jobposted',function(req,res){

        res.json([{

            postdate_id : "1",
            description : "Today"

        },{

            postdate_id : "2",
            description : "Yesterday"

        },{

            postdate_id : "3",
            description : "One Week"

        },{

            postdate_id : "4",
            description : "Two Weeks"

        },{

            postdate_id : "5",
            description : "Three Weeks"

        },{

            postdate_id : "6",
            description : "One Month"

        },{

            postdate_id : "7",
            description : "Two Months"

        },{

            postdate_id : "8",
            description : "Three Months"

        }]);

    });


}