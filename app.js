// Module Dependencies and Setup

var express = require('express')
    , mongoose = require('mongoose')
    , UserModel = require('./models/UserModel')
    , User = mongoose.model('User')
    , cors = require('cors')
    , fs = require('fs')
    , welcome = require('./businesslogic/welcome')
    , users = require('./businesslogic/users')
    , http = require('http')
    , path = require('path')
    , engine = require('ejs-locals')
    , flash = require('connect-flash')
    , passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy
    , expressValidator = require('express-validator')
    , db = require('./config/db')
    , app = express();

app.engine('ejs', engine);
app.set('port', process.env.PORT || 8001);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(expressValidator);
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
//app.use(cors());

// Helpers

app.use(function (req, res, next) {
    res.locals.userIsAuthenticated = req.isAuthenticated(); // check for user authentication
    res.locals.user = req.user; // make user available in all views
    res.locals.errorMessages = req.flash('error'); // make error alert messages available in all views
    res.locals.successMessages = req.flash('success'); // make success messages available in all views
    app.locals.layoutPath = "../shared/layout";
    //app.locals.layoutPath = "../temptest/layout";



    next();
});


// Routing Initializers

app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

// Error Handling

if ('development' == app.get('env')) {
    app.use(express.errorHandler());
} else {
    app.use(function (err, req, res, next) {
        res.render('errors/500', {status: 500});
    });
}

// Database Connection

if ('development' == app.get('env')) {
//    mongoose.connect('mongodb://localhost/nodedemo');
} else {
    // insert db connection for production
}

// Authentication

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

//passport.use(new LocalStrategy(
//    function (username, password, done) {
//        User.findOne({ username: username }, function (err, user) {
//            if (err) return done(err);
//            if (!user) return done(null, false, { message: "Sorry, we don't recognize that username." });
//            user.validPassword(password, function (err, isMatch) {
//                if (err) return done(err);
//                if (isMatch) return done(null, user);
//                else done(null, false, { message: 'Incorrect password.' });
//            });
//        });
//    }
//));

passport.use(new LocalStrategy(
    {usernameField: 'username', passReqToCallback: true},
    function (req, username, password, done) {
        User.findOne({username: username, type: req.body.type}, function (err, user) {
            if (err) return done(err);
            if (!user) return done(null, false, {message: "Sorry, we don't recognize that username."});
            user.validPassword(password, function (err, isMatch) {
                if (err) return done(err);
                if (isMatch) return done(null, user);
                else done(null, false, {message: 'Incorrect password.'});
            });
        });
    }
));


// dynamically include routes (Controller)
fs.readdirSync('./controllers').forEach(function (file) {
    if (file.substr(-3) == '.js') {
        var route = require('./controllers/' + file);

        app.get('/', function(req, res){
            //res.render('companies/new');
            res.redirect('/register');

        });
        route.controller(app);
    }
});


http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});