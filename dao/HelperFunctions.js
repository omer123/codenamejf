/**
 * Created by Rizwan on 9/28/14.
 */


// FOR MOBILE APPS

exports.getAll = function(query, collection, stringModelName, res){
    collection.find(function(err, collectionsListFromRepo){
        if(collectionsListFromRepo){
            if(err){
                res.statusCode = 500;
                res.send({'Response-Description': err});
            }
            else{
                res.json(collectionsListFromRepo);
            }
        }
        else{
            res.statusCode = 404;
            res.send({'Response-Description': stringModelName + ' Not Found'});
        }
    });
}


exports.getOne = function(query, collection, stringModelName, res){
    collection.findOne(query,function(err, collectionsFromRepo){
        if(collectionsFromRepo){
            if(err){
                res.statusCode = 500;
                res.send({'Response-Description': err});
            }
            else{
                res.json(collectionsFromRepo);
            }
        }
        else{
            res.statusCode = 404;
            res.send({'Response-Description': stringModelName + ' Not Found'});
        }
    });
}