/**
 * Created by SIGMA on 2/1/14.
 */
// Bring Mongoose into the app
var mongoose = require('mongoose');

// Build the connection string

// New DB Update

// Env Variables for Mongo DB Connection String
var dbURI = process.env.DBURI;
if (dbURI == null) {
    dbURI = 'mongodb://localhost/jobfinder';
//    dbURI = 'mongodb://rizi2:rizi2@ds041160.mongolab.com:41160/jobfinder';
//    dbURI = 'mongodb://riziboy:riziboy@ds053080.mongolab.com:53080/jobfinder2';
}

// Create the database connection
mongoose.connect(dbURI);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + dbURI);

});
// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});


//mongoose.set('debug', true);

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(o);
    });
});