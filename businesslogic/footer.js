var mongoose = require('mongoose') 
    , User = require('../models/UserModel')
    , Country = require('../models/CountryModel')
    , City = require('../models/CityModel')
    , Region = require('../models/RegionModel')
    , JobSector = require('../models/JobSectorModel')
    , passport = require('passport')
    , async = require('async')
    , random = require('../utilities/random')
    , contactUsModel = require('../models/ContactUsModel')
    , changeCase = require('change-case');

// FOOTER SECTION
exports.privacypolicy = function(req, res){
    res.render('footer/privacypolicy');
}

exports.termsandconditions = function(req, res){
    res.render('footer/termsandconditions');
}

exports.aboutus = function(req, res){
    res.render('footer/about');
}

exports.contactus = function(req, res) {

    Country.find().sort('countryname').exec(function (err, countries) {
        if (err) return next(err);
        if (countries.length > 0) {

            for(var i=0; i<countries.length; i++){
                countries[i].countryname = countries[i].countryname.toUpperCase(); //changeCase.title(countries[i].countryname);
            }
            res.render('footer/contactus' , {contactus: new contactUsModel({}),   countries: countries });

        } else {
            req.flash('error', "Please add some Countries first from Admin Panel.");
            return res.redirect('/activejobfinder/web/contactus');
        }
    });

}


// Create contact
exports.contactusCreate = function (req, res, next) {
    var newContact = new contactUsModel(req.body);
    newContact.contact_id = random.randomGUID();
    console.log(newContact);
    newContact.save(function (err) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('footer/contactus', {contact: newContact, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "Submitted. Thanks for your query!");
        return res.redirect('/activejobfinder/web/contactus');
    });
}

 
exports.queries = function (req, res, next) {


    contactUsModel.find(function (err, queriesList) {
        if (err) return next(err);

        async.forEach(queriesList, function (queriesResponse, callback) {


                Country.findOne({country_id: queriesResponse.country_id}, function (err, countryFromRepo) {

                    if (!err) {
                        queriesResponse.country_id = countryFromRepo.countryname;
                        callback();
                    }
                    else {
                        callback(err);
                    }
                });

            },
            function (err) {
                if (err) {
                    return next(err);
                } else {
                    res.render('footer/queries', {
                        queries: queriesList
                    });
                }

            });
    });
}





