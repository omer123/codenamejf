var mongoose = require('mongoose')
    , Job = require('../models/JobModel')
    , User = require('../models/UserModel')
    , JobType = require('../models/JobTypeModel')
    , JobPost = require('../models/JobPostModel')
    , Currency = require('../models/CurrencyModel')
    , passport = require('passport')
    , async = require('async')
    , random = require('../utilities/random');

// Get dashboard
exports.dashboard = function (req, res) {
    res.render('jobs/dashboard');
}

// Get registration page
exports.register = function (req, res) {
    JobType.find(function (errJobSector, jobtypes) {
        if (errJobSector) {
            req.flash('error', errJobSector);
            return res.redirect('/activejobfinder/web/users/dashboard');
        }
        else if (jobtypes.length > 0) {
            Currency.find(function (err, currencies) {
                if (err) return next(err);
                if (currencies.length > 0) {
                    res.render('jobs/new', {jobs: new Job({}), jobpost: new JobPost({}), currencies: currencies, jobtypes: jobtypes});
                } else {
                    req.flash('error', "Please add some Currencies first from Admin Panel.");
                    return res.redirect('/activejobfinder/web/users/dashboard');
                }
            });
        } else {
            req.flash('error', "Please add some Job Types first from Admin Panel.");
            return res.redirect('/activejobfinder/web/users/dashboard');
        }
    });
}

// Account page
exports.account = function (req, res) {
    res.render('jobs/edit');
}

// List all jobs
exports.list = function (req, res, next) {
    Job.find(function (err, jobsList) {
        if (err) return next(err);
        var jobsResponseList = [];
        for (var i = 0; i < jobsList.length; i++) {
            // Fill Job Entity
            if (jobsList[i].jobtitle == 'Ltd') {
                jobsList[i].jobtitle = 'Private job limited by shares';
            } else if (jobsList[i].jobtitle == 'Llp') {
                jobsList[i].jobtitle = 'Limited liability partnership';
            } else if (jobsList[i].jobtitle == 'Llc') {
                jobsList[i].jobtitle = 'Limited liability job';
            } else if (jobsList[i].jobtitle == 'Plc') {
                jobsList[i].jobtitle = 'Public limited job';
            } else if (jobsList[i].jobtitle == 'S-Corp') {
                jobsList[i].jobtitle = 'S corporation';
            } else if (jobsList[i].jobtitle == 'C-Corp') {
                jobsList[i].jobtitle = 'C corporation';
            } else {
                jobsList[i].jobtitle = 'N/A';
            }

            jobsResponseList.push(jobsList[i]);
        }

        async.forEach(jobsResponseList, function (jobsResponse, callback) {

                async.parallel([
                        //GET Countries
                        function (callback) {
                            Country.findOne({country_id: jobsResponse.country_id}, function (err, countryFromRepo) {

                                if (!err) {
                                    jobsResponse.country_id = countryFromRepo.countryname;
                                    callback();
                                }
                                else {
                                    callback(err);
                                }
                            });
                        },
                        //GET Regions
                        function (callback) {
                            Region.findOne({region_id: jobsResponse.region_id}, function (err, regionFromRepo) {

                                if (!err) {
                                    jobsResponse.region_id = regionFromRepo.regionname;
                                    callback();
                                }
                                else {
                                    callback(err);
                                }
                            });
                        },
                        //Get Cities
                        function (callback) {
                            City.findOne({city_id: jobsResponse.city_id}, function (err, cityFromRepo) {

                                if (!err) {
                                    jobsResponse.city_id = cityFromRepo.cityname;
                                    callback();
                                }
                                else {
                                    callback(err);
                                }
                            });
                        }

                    ],
                    function (err) {
                        if (err) callback(err);
                        callback();
                    }
                );

            },
            function (err) {
                if (err) {
                    return next(err);
                } else {
                    res.render('jobs/index', {
                        jobs: jobsResponseList
                    });
                }

            });
    });
}

// Update job
exports.update = function (req, res, next) {
    var job = req.job;

    // use save instead of update to trigger 'save' event for password hashing
    job.set(req.body);
    job.save(function (err, job) {

        // Uniqueness and Save Validations

        if (err && err.code == 11001) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.redirect('/activejobfinder/web/jobs/account');
        }
        if (err) return next(err);

        // Job updated successfully, redirecting

        req.flash('success', "Account updated successfully.");
        return res.redirect('/activejobfinder/web/jobs/account');


    });
}

// Create job
exports.create = function (req, res, next) {
    var newJob = new Job(req.body);
    newJob.job_id = random.randomGUID();
    console.log(newJob);
    newJob.save(function (err, job) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('jobs/new', {job: newJob, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "Job Profile created successfully!");
        return res.redirect('/activejobfinder/web/jobs');
    });
}

// Validations for job objects upon job update or create
exports.jobValidations = function (req, res, next) {
    var creatingJob = req.url == "/activejobfinder/web/jobs/register";
    req.assert('jobname', 'Jobname is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingJob) return res.render('jobs/new', {job: new Job(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/jobs/account");
    } else next();
}
