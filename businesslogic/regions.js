var mongoose = require('mongoose')
    , Region = require('../models/RegionModel')
    , Country = require('../models/CountryModel')//mongoose.model('Region')
    , passport = require('passport')
    , random = require('../utilities/random')
    , async = require('async')
    , sort = require('../utilities/sort')
    , changeCase = require('change-case');


// Get dashboard
exports.dashboard = function (req, res) {
    res.render('regions/dashboard');
}


exports.deleteRegion = function (req, res) {
    Region.remove({region_id: req.params.regionid}, function (err) {
        if (!err) {
//            req.flash('success', "Region Deleted successfully!");
//            return res.redirect('/activejobfinder/web/regions');
            res.statusCode = 200;
            res.end();
        }
        else {
//            req.flash('error', err);
//            return res.redirect('/activejobfinder/web/regions');
            res.statusCode=500;
            res.json(err);
        }
    });
}


// Get registration page
exports.register = function (req, res) {
    Country.find(function (errCountry, countries) {
        if (errCountry) return next(errCountry);

        if (countries.length > 0) {
            res.render('regions/new', {
                region: new Region({}), countries: countries
            });
        } else {
            req.flash('error', "Please add some Countries first.");
            return res.redirect('/activejobfinder/web/users/dashboard');
        }


    });

}


// Account page
exports.account = function (req, res) {
    Country.find(function (errCountry, countries) {
        if (errCountry) return next(errCountry);

        if (countries.length > 0) {
            Region.findOne({region_id: req.params.regionid}).exec(function (err, RegionFromRepo) {
                if (err) {
                    // Send Request Error Flash
                    req.flash('error', err);
                    return res.redirect('/activejobfinder/web/regions');
                }
                else {
                    // Fill the update page with the data
                    res.render('regions/edit', {region: RegionFromRepo, countries: countries});
                }
            });

        } else {
            req.flash('error', "Please add some Countries first.");
            return res.redirect('/activejobfinder/web/users/dashboard');
        }


    });

}

// Get list by Country ID
exports.getRegionsByCountryId = function (req, res) {

    Region.find({country_id: req.params.countryid}).sort('regionname').exec(function (err, RegionFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            for (var i = 0; i < RegionFromRepo.length; i++) {
                RegionFromRepo[i].regionname = RegionFromRepo[i].regionname.toUpperCase();//changeCase.title(RegionFromRepo[i].regionname);
            }

            res.json(RegionFromRepo);
        }
    });
}

// List all regions
exports.list = function (req, res, next) {

    var regionsResponseList = [];
    Region.find(function (err, regionsFromRepoList) {
        if (err) return next(err);


        for (var i = 0; i < regionsFromRepoList.length; i++) {

            // Fill Type
            if (regionsFromRepoList[i].type == '0') {
                regionsFromRepoList[i].type = 'Region';
            } else if (regionsFromRepoList[i].type == '1') {
                regionsFromRepoList[i].type = 'State';
            } else if (regionsFromRepoList[i].type == '2') {
                regionsFromRepoList[i].type = 'Province';
            } else if (regionsFromRepoList[i].type == '3') {
                regionsFromRepoList[i].type = 'District';
            } else {
                regionsFromRepoList[i].type = 'N/A';
            }

            regionsResponseList.push(regionsFromRepoList[i]);
        }

        async.forEach(regionsResponseList, function (regionsResponse, callback) {

                Country.findOne({country_id: regionsResponse.country_id}, function (errCountry, countryFromRepo) {
                    if (errCountry) return next(errCountry);
                    if (countryFromRepo) {
                        // Fill Countries Collections
                        regionsResponse['countryname'] = countryFromRepo.countryname;
                        callback();
                    }
                    else{
                        regionsResponse['countryname'] = 'N/A';
                        callback();
                    }
                });
            },
            function (err) {
                if (err) {
                    return next(err);
                } else {
                    sort.asc(regionsResponseList,'regionname');
                    res.render('regions/index', {
                        regions: regionsResponseList
                    });
                }

            });

    });
}


// Update region
exports.update = function (req, res, next) {
    console.log('Request Region : ' + JSON.stringify(req.body));

    Region.findOne({region_id: req.body.region_id}, function (err, regionFromRepo) {

        if (req.body.type != null) {
            regionFromRepo.type = req.body.type
        }
        if (req.body.regionname != null) {
            regionFromRepo.regionname = req.body.regionname
        }
        if (req.body.country_id != null) {
            regionFromRepo.country_id = req.body.country_id
        }

        if (!err) {
            regionFromRepo.save(function (err) {
                if (!err) {
                    req.flash('success', "Record updated successfully.");
                    return res.redirect('/activejobfinder/web/regions');
                }
                else {
                    req.flash('error', err);
                    return res.redirect('/activejobfinder/web/regions');
                }
            });
        } else {
            req.flash('error', err);
            return res.redirect('/activejobfinder/web/regions');
        }
    });


//    var region = req.region;
//
//    // use save instead of update to trigger 'save' event for password hashing
//    region.set(req.body);
//    region.save(function (err, region) {
//
//        // Uniqueness and Save Validations
//
//        if (err && err.code == 11001) {
//            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
//            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
//            return res.redirect('/activejobfinder/web/regions/account');
//        }
//        if (err) return next(err);
//
//        // Region updated successfully, redirecting
//
//        req.flash('success', "Account updated successfully.");
//        return res.redirect('/activejobfinder/web/regions/account');
//
//
//    });
}

// Create region
exports.create = function (req, res, next) {
    var newRegion = new Region(req.body);
    newRegion.region_id = random.randomGUID();
    newRegion.save(function (err, region) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('regions/new', {region: newRegion, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "Region Profile created successfully!");
        return res.redirect('/activejobfinder/web/regions');
    });
}

// Validations for region objects upon region update or create
exports.regionValidations = function (req, res, next) {
    var creatingRegion = req.url == "/activejobfinder/web/regions/register";
    req.assert('regionname', 'Regionname is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingRegion) return res.render('regions/new', {region: new Region(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/regions/account");
    } else next();
}
