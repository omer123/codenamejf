// Get homepage
exports.index = function(req, res){
  if(req.isAuthenticated()) return res.redirect('/activejobfinder/web/users/dashboard');
  res.render('welcome/index');
}

exports.index_login = function(req, res){
//    if(req.isAuthenticated()) return res.redirect('/activejobfinder/web/users/dashboard');
    res.render('welcome/index_cms');
}


exports.home = function(req, res){
    //res.render('welcome/underconstruction');
    // temptest
    res.render('temptest/home');
}
exports.underconstruction = function(req, res){
    res.render('welcome/underconstruction');
}
// Handle 404 gracefully
exports.not_found = function(req, res){
  req.flash('error', "That doesn't seem to be a page.");
  res.redirect('/');
}