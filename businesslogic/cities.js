var mongoose = require('mongoose')
    , City = require('../models/CityModel')
    , Country = require('../models/CountryModel')
    , Region = require('../models/RegionModel')//mongoose.model('City')
    , passport = require('passport')
    , random = require('../utilities/random')
    , async = require('async')
    , sort = require('../utilities/sort')
    , changeCase = require('change-case');


// Get dashboard
exports.dashboard = function (req, res) {
    res.render('cities/dashboard');
}


exports.deleteCity = function (req, res) {
    City.remove({city_id: req.params.cityid}, function (err) {
        if (!err) {
            res.statusCode = 200;
            res.end();
        }
        else {
            res.statusCode = 500;
            res.json(err);
        }
    });
}


// Get registration page
exports.register = function (req, res) {

    Region.find(function (err, regions) {
        if (err) return next(err);
        if (regions.length > 0) {

            async.forEach(regions, function (region, callback) {

                    Country.findOne({country_id: region.country_id}, function (err, countryFromRepo) {
                        if (err) return next(err);
                        if (countryFromRepo) {

                            // Fill Countries Collections
                            region.regionname = countryFromRepo.countryname + ' - ' + region.regionname;

                            callback();
                        }
                    });

                },
                function (err) {
                    if (err) {
                        return next(err);
                    } else {
                        console.log(regions);
                        sort.asc(regions, 'regionname');
                        res.render('cities/new', {
                            regions: regions, city: new City({})
                        });
                    }

                });


        } else {
            req.flash('error', "Please add some Regions first.");
            return res.redirect('/activejobfinder/web/users/dashboard');
        }
    });

}


// Account page
exports.account = function (req, res) {
    Region.find(function (err, regions) {
        if (err) return next(err);
        if (regions.length > 0) {

            City.findOne({city_id: req.params.cityid}).exec(function (err, CityFromRepo) {
                if (CityFromRepo) {
                    async.forEach(regions, function (region, callback) {

                            Country.findOne({country_id: region.country_id}, function (err, countryFromRepo) {
                                if (err) return next(err);
                                if (countryFromRepo) {

                                    // Fill Countries Collections
                                    region.regionname = countryFromRepo.countryname + ' - ' + region.regionname;

                                    callback();
                                }
                            });

                        },
                        function (err) {
                            if (err) {
                                return next(err);
                            } else {
                                console.log(regions);
                                sort.asc(regions, 'regionname');

                                res.render('cities/edit', {
                                    regions: regions, city: CityFromRepo
                                });

                            }
                        });
                }
                else {
                    req.flash('error', "No City Found.");
                    return res.redirect('/activejobfinder/web/cities');
                }
            });
        } else {
            req.flash('error', "Please add some Regions first.");
            return res.redirect('/activejobfinder/web/cities');
        }
    });

}


// Get list by Region ID
exports.getCitiesByRegionId = function (req, res) {

    City.find({region_id: req.params.regionid}).sort('cityname').exec(function (err, CityFromRepo) {
        if (err) {
            res.statusCode = 500;
            res.end();
        }
        else {
            for (var i = 0; i < CityFromRepo.length; i++) {
                CityFromRepo[i].cityname = CityFromRepo[i].cityname.toUpperCase();//changeCase.title(CityFromRepo[i].cityname);
            }

            res.json(CityFromRepo);
        }
    });
}

// List all cities
exports.list = function (req, res, next) {
    var citiesResponseList = [];
    City.find(function (err, citiesFromRepoList) {
        if (err) return next(err);


        for (var i = 0; i < citiesFromRepoList.length; i++) {
            // Fill Type
            if (citiesFromRepoList[i].type == '0') {
                citiesFromRepoList[i].type = 'Settlement';
            } else if (citiesFromRepoList[i].type == '1') {
                citiesFromRepoList[i].type = 'City';
            } else if (citiesFromRepoList[i].type == '2') {
                citiesFromRepoList[i].type = 'Local Government Area';
            } else if (citiesFromRepoList[i].type == '3') {
                citiesFromRepoList[i].type = 'District';
            } else {
                citiesFromRepoList[i].type = 'N/A';
            }

            citiesResponseList.push(citiesFromRepoList[i]);
        }

        async.forEach(citiesResponseList, function (citiesResponse, callback) {

                Region.findOne({region_id: citiesResponse.region_id}, function (errRegion, regionFromRepo) {
                    if (errRegion) return next(errRegion);
                    if (regionFromRepo) {

                        Country.findOne({country_id: regionFromRepo.country_id}, function (err, countryFromRepo) {
                            if (err) return next(err);
                            if (countryFromRepo) {

                                // Fill Countries Collections
                                citiesResponse['regionname'] = countryFromRepo.countryname + ' - ' + regionFromRepo.regionname;
                                callback();
                            }
                            else{
                                citiesResponse['regionname'] ='N/A';
                                callback();
                            }

                        });

                    }
                    else{
                        callback();
                    }

                });
            },
            function (err) {
                if (err) {
                    return next(err);
                } else {
                    sort.asc(citiesResponseList,'cityname');
//                    console.log(citiesFromRepoList)
                    res.render('cities/index', {
                        cities: citiesResponseList
                    });
                }

            });

    });


}

// Update city
exports.update = function (req, res, next) {
    City.findOne({city_id: req.body.city_id}, function (err, cityFromRepo) {

        if (req.body.cityname != null) {
            cityFromRepo.cityname = req.body.cityname
        }
        if (req.body.region_id != null) {
            cityFromRepo.region_id = req.body.region_id
        }
        if (req.body.type != null) {
            cityFromRepo.type = req.body.type
        }

        if (!err) {
            cityFromRepo.save(function (err) {
                if (!err) {
                    req.flash('success', "Record updated successfully.");
                    return res.redirect('/activejobfinder/web/cities');
                }
                else {
                    req.flash('error', err);
                    return res.redirect('/activejobfinder/web/cities');
                }
            });
        } else {
            req.flash('error', err);
            return res.redirect('/activejobfinder/web/cities');
        }
    });
//    var city = req.city;
//
//    // use save instead of update to trigger 'save' event for password hashing
//    city.set(req.body);
//    city.save(function (err, city) {
//
//        // Uniqueness and Save Validations
//
//        if (err && err.code == 11001) {
//            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
//            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
//            return res.redirect('/activejobfinder/web/cities/account');
//        }
//        if (err) return next(err);
//
//        // City updated successfully, redirecting
//
//        req.flash('success', "Account updated successfully.");
//        return res.redirect('/activejobfinder/web/cities/account');
//
//
//    });
}

// Create city
exports.create = function (req, res, next) {
    var newCity = new City(req.body);
    newCity.city_id = random.randomGUID();
    newCity.save(function (err, city) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('cities/new', {city: newCity, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "City Profile created successfully!");
        return res.redirect('/activejobfinder/web/cities');
    });
}

// Validations for city objects upon city update or create
exports.cityValidations = function (req, res, next) {
    var creatingCity = req.url == "/activejobfinder/web/cities/register";
    req.assert('cityname', 'Cityname is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingCity) return res.render('cities/new', {city: new City(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/cities/account");
    } else next();
}
