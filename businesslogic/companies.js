var mongoose = require('mongoose')
    , Company = require('../models/CompanyModel')
    , User = require('../models/UserModel')
    , Country = require('../models/CountryModel')
    , City = require('../models/CityModel')
    , Region = require('../models/RegionModel')
    , JobSector = require('../models/JobSectorModel')
    , passport = require('passport')
    , async = require('async')
    , random = require('../utilities/random')
    , app_config = require('../config/app_config')
    , changeCase = require('change-case');

// Get dashboard
exports.dashboard = function (req, res) {
    res.render('companies/dashboard');
}

// Get registration page
exports.register = function (req, res) {
//    res.render('companies/new', {company: new Company({})});
    JobSector.find().sort('businesssector').exec(function (errJobSector, jobsectors) {
        if (errJobSector) {
            req.flash('error', errJobSector);
            return res.redirect('/activejobfinder/web/users/dashboard');
        }
        else if (jobsectors.length > 0) {

            for (var i = 0; i < jobsectors.length; i++) {
                jobsectors[i].businesssector = jobsectors[i].businesssector.toUpperCase();// changeCase.upperCaseFirst(jobsectors[i].businesssector);
            }

            Country.find().sort('countryname').exec(function (err, countries) {
                if (err) return next(err);
                if (countries.length > 0) {

                    for (var i = 0; i < countries.length; i++) {
                        countries[i].countryname = countries[i].countryname; //changeCase.title(countries[i].countryname);
                    }


                    Region.find(function (err, regions) {
                        if (err) return next(err);
                        if (regions.length > 0) {
                            City.find(function (err, cities) {
                                if (err) return next(err);
                                if (regions.length > 0) {
                                    res.render('companies/new', {
                                        users: new User({}),
                                        companies: new Company(),
                                        cities: cities,
                                        regions: regions,
                                        countries: countries,
                                        jobsectors: jobsectors
                                    });
                                } else {
                                    req.flash('error', "Please add some Regions first from Admin Panel.");
                                    return res.redirect('/activejobfinder/web/users/dashboard');
                                }
                            });
                        } else {
                            req.flash('error', "Please add some Regions first from Admin Panel.");
                            return res.redirect('/activejobfinder/web/users/dashboard');
                        }
                    });


                } else {
                    req.flash('error', "Please add some Countries first from Admin Panel.");
                    return res.redirect('/activejobfinder/web/users/dashboard');
                }
            });
        } else {
            req.flash('error', "Please add some Job/Business Sectors first from Admin Panel.");
            if (req.params.loginOrCompany == 1) {

                return res.redirect('/activejobfinder/web/users/dashboard');
            }
            else {
                return res.redirect('/activejobfinder/web/users/login');
            }

        }
    });
}

// Account page
exports.account = function (req, res) {
    res.render('companies/edit');
}

// List all companies
exports.list = function (req, res, next) {
    Company.find(function (err, companiesList) {
        if (err) return next(err);
        var companiesResponseList = [];
        for (var i = 0; i < companiesList.length; i++) {
            // Fill Company Entity
            if (companiesList[i].companytitle == 'Ltd') {
                companiesList[i].companytitle = 'Private company limited by shares';
            } else if (companiesList[i].companytitle == 'Llp') {
                companiesList[i].companytitle = 'Limited liability partnership';
            } else if (companiesList[i].companytitle == 'Llc') {
                companiesList[i].companytitle = 'Limited liability company';
            } else if (companiesList[i].companytitle == 'Plc') {
                companiesList[i].companytitle = 'Public limited company';
            } else if (companiesList[i].companytitle == 'S-Corp') {
                companiesList[i].companytitle = 'S corporation';
            } else if (companiesList[i].companytitle == 'C-Corp') {
                companiesList[i].companytitle = 'C corporation';
            } else {
                companiesList[i].companytitle = 'N/A';
            }

            companiesResponseList.push(companiesList[i]);
        }

        async.forEach(companiesResponseList, function (companiesResponse, callback) {

                async.parallel([
                        //GET Countries
                        function (callback) {
                            companiesResponse.logo_image = /*'http://activejobfinder.com:8004/activejobfinder/web/company/images/'*/
                                app_config.companyLogoBaseURL + companiesResponse.company_id;

                            Country.findOne({country_id: companiesResponse.country_id}, function (err, countryFromRepo) {

                                if (!err) {
                                    if (countryFromRepo) {
                                        companiesResponse.country_id = countryFromRepo.countryname;
                                        callback();
                                    }
                                    else {
                                        companiesResponse.country_id = 'N/A';
                                        callback();
                                    }
                                }
                                else {
                                    callback(err);
                                }
                            });
                        },
                        //GET Regions
                        function (callback) {
                            Region.findOne({region_id: companiesResponse.region_id}, function (err, regionFromRepo) {

                                if (!err) {
                                    if (regionFromRepo) {
                                        companiesResponse.region_id = regionFromRepo.regionname;
                                        callback();
                                    }
                                    else {
                                        companiesResponse.region_id = 'N/A'
                                        callback();
                                    }
                                }
                                else {
                                    callback(err);
                                }
                            });
                        },
                        //Get Cities
                        function (callback) {
                            City.findOne({city_id: companiesResponse.city_id}, function (err, cityFromRepo) {

                                if (!err) {
                                    if (cityFromRepo) {

                                        companiesResponse.city_id = cityFromRepo.cityname;
                                        callback();
                                    }
                                    else {

                                        companiesResponse.city_id = 'N/A';
                                        callback();
                                    }
                                }
                                else {
                                    callback(err);
                                }
                            });
                        }

                    ],
                    function (err) {
                        if (err) callback(err);
                        callback();
                    }
                );

            },
            function (err) {
                if (err) {
                    return next(err);
                } else {
                    res.render('companies/index', {
                        companies: companiesResponseList
                    });
                }

            });
    });
}

// Update company
exports.update = function (req, res, next) {
    var company = req.company;

    // use save instead of update to trigger 'save' event for password hashing
    company.set(req.body);
    company.save(function (err, company) {

        // Uniqueness and Save Validations

        if (err && err.code == 11001) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.redirect('/activejobfinder/web/companies/account');
        }
        if (err) return next(err);

        // Company updated successfully, redirecting

        req.flash('success', "Account updated successfully.");
        return res.redirect('/activejobfinder/web/companies/account');


    });
}

// Create company
exports.create = function (req, res, next) {
    var newCompany = new Company(req.body);
    newCompany.company_id = random.randomGUID();
    console.log(newCompany);
    newCompany.save(function (err, company) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('companies/new', {company: newCompany, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "Company Profile created successfully!");
        return res.redirect('/');
    });
}

// Validations for company objects upon company update or create
exports.companyValidations = function (req, res, next) {
    var creatingCompany = req.url == "/activejobfinder/web/companies/register";
    req.assert('companyname', 'Companyname is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingCompany) return res.render('companies/new', {
            company: new Company(req.body),
            errorMessages: req.flash('error')
        });
        // Update handling if errors present
//        else return res.redirect("/activejobfinder/web/companies/account");

        else {
            req.flash('success', "Company Profile created successfully!");
            return res.render('/', {successMessages: req.flash('success')});
        }
    } else next();
}
