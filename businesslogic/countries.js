var mongoose = require('mongoose')
    , Country = require('../models/CountryModel')
    , Currency = require('../models/CurrencyModel')//mongoose.model('Country')
    , passport = require('passport')
    , random = require('../utilities/random')
    , async = require('async');


// Get dashboard
exports.dashboard = function (req, res) {
    res.render('countries/dashboard');
}


exports.deleteCountry = function (req, res) {
    Country.remove({country_id: req.params.countryid}, function (err) {
        if (!err) {
            res.statusCode = 200;
            res.end();
        }
        else {
            res.statusCode = 500;
            res.json(err);
        }
    });
}


// Get registration page
exports.register = function (req, res) {
    Currency.find(function (err, currencies) {
        if (err) return next(err);
        if (currencies.length > 0) {
            res.render('countries/new', {
                currencies: currencies, country: new Country({})
            });
        } else {
            req.flash('error', "Please add some Currency first.");
            return res.redirect('/activejobfinder/web/users/dashboard');
        }
    });
}

// Account page
exports.account = function (req, res) {
    Currency.find(function (err, currencies) {
        if (err) return next(err);
        if (currencies.length > 0) {
            Country.findOne({country_id: req.params.countryid}, function (err, countryFromRepo) {

                if (countryFromRepo) {
                    res.render('countries/edit', {
                        currencies: currencies, country: countryFromRepo
                    });
                }
                else {
                    req.flash('error', "No Country Found.");
                    return res.redirect('/activejobfinder/web/countries');
                }
            });

        } else {
            req.flash('error', "Please add some Currency first.");
            return res.redirect('/activejobfinder/web/countries');
        }
    });
}

// List all countries
exports.list = function (req, res, next) {

    var countriesResponseList = [];
    Country.find(function (err, countriesFromRepoList) {
        if (err) return next(err);


        for (var i = 0; i < countriesFromRepoList.length; i++) {

            // Fill Type
            if (countriesFromRepoList[i].type == '0') {
                countriesFromRepoList[i].type = 'Region';
            } else if (countriesFromRepoList[i].type == '1') {
                countriesFromRepoList[i].type = 'State';
            } else if (countriesFromRepoList[i].type == '2') {
                countriesFromRepoList[i].type = 'Province';
            } else {
                countriesFromRepoList[i].type = 'N/A';
            }

            countriesResponseList.push(countriesFromRepoList[i]);
        }

        async.forEach(countriesResponseList, function (countriesResponse, callback) {

                Currency.findOne({currency_id: countriesResponse.currency_id}, function (errCountry, currencyFromRepo) {
                    if (errCountry) return next(errCountry);
                    if (currencyFromRepo) {
                        // Fill Countries Collections
                        countriesResponse['currencyname'] = currencyFromRepo.currencyname + ' (' + currencyFromRepo.sign + ')';
                        callback();
                    }
                    else{
                        countriesResponse['currencyname'] = 'N/A';
                        callback();
                    }
                });
            },
            function (err) {
                if (err) {
                    return next(err);
                } else {
                    res.render('countries/index', {
                        countries: countriesResponseList
                    });
                }

            });

    });


}

// Update country
exports.update = function (req, res, next) {
    Country.findOne({country_id: req.body.country_id}, function (err, countryFromRepo) {

        if (req.body.countryname != null) {
            countryFromRepo.countryname = req.body.countryname
        }
        if (req.body.currency_id != null) {
            countryFromRepo.currency_id = req.body.currency_id
        }

        if (!err) {
            countryFromRepo.save(function (err) {
                if (!err) {
                    req.flash('success', "Record updated successfully.");
                    return res.redirect('/activejobfinder/web/countries');
                }
                else {
                    req.flash('error', err);
                    return res.redirect('/activejobfinder/web/countries');
                }
            });
        } else {
            req.flash('error', err);
            return res.redirect('/activejobfinder/web/countries');
        }
    });

//    var country = req.country;
//
//    // use save instead of update to trigger 'save' event for password hashing
//    country.set(req.body);
//    country.save(function (err, country) {
//
//        // Uniqueness and Save Validations
//
//        if (err && err.code == 11001) {
//            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
//            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
//            return res.redirect('/activejobfinder/web/countries/account');
//        }
//        if (err) return next(err);
//
//        // Country updated successfully, redirecting
//
//        req.flash('success', "Account updated successfully.");
//        return res.redirect('/activejobfinder/web/countries/account');
//
//
//    });
}

// Create country
exports.create = function (req, res, next) {
    var newCountry = new Country(req.body);
    newCountry.country_id = random.randomGUID();
    newCountry.save(function (err, country) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('countries/new', {country: newCountry, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "Country Profile created successfully!");
        return res.redirect('/activejobfinder/web/countries');
    });
}

// Validations for country objects upon country update or create
exports.countryValidations = function (req, res, next) {
    var creatingCountry = req.url == "/activejobfinder/web/countries/register";
    req.assert('countryname', 'Countryname is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingCountry) return res.render('countries/new', {country: new Country(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/countries/account");
    } else next();
}
