var mongoose = require('mongoose')
    , JobSector = require('../models/JobSectorModel')
    , JobBand = require('../models/JobBandModel')//mongoose.model('JobSector')
    , passport = require('passport')
    , random = require('../utilities/random')
    , async = require('async');


// Get dashboard
exports.dashboard = function (req, res) {
    res.render('jobsectors/dashboard');
}


// Get registration page
exports.register = function (req, res) {
    JobBand.find(function (err, jobbands) {
        if (err) return next(err);
        if (jobbands.length > 0) {
            res.render('jobsectors/new', {jobsector: new JobSector({}), jobbands: jobbands});
        } else {
            req.flash('error', "Please add some Job Bands first.");
            return res.redirect('/activejobfinder/web/users/dashboard');
        }
    });
}


// Account page
exports.account = function (req, res) {
    res.render('jobsectors/edit');
}

// List all jobsectors
exports.list = function (req, res, next) {


    var jobsectorsResponseList = [];
    JobSector.find(function (err, jobsectorsFromRepoList) {
        if (err) return next(err);


        for (var i = 0; i < jobsectorsFromRepoList.length; i++) {

            // Fill Type
            if (jobsectorsFromRepoList[i].type == '0') {
                jobsectorsFromRepoList[i].type = 'JobSector';
            } else if (jobsectorsFromRepoList[i].type == '1') {
                jobsectorsFromRepoList[i].type = 'State';
            } else if (jobsectorsFromRepoList[i].type == '2') {
                jobsectorsFromRepoList[i].type = 'Province';
            } else {
                jobsectorsFromRepoList[i].type = 'N/A';
            }

            jobsectorsResponseList.push(jobsectorsFromRepoList[i]);
        }

        async.forEach(jobsectorsResponseList, function (jobsectorsResponse, callback) {

                JobBand.findOne({jobband_id: jobsectorsResponse.jobband_id}, function (errJobBand, jobbandFromRepo) {
                    if (errJobBand) return next(errJobBand);
                    if (jobbandFromRepo) {
                        // Fill Countries Collections
                        jobsectorsResponse['jobbandname'] = jobbandFromRepo.jobbandname;
                        callback();
                    }
                });
            },
            function (err) {
                if (err) {
                    return next(err);
                } else {
                    res.render('jobsectors/index', {
                        jobsectors: jobsectorsResponseList
                    });
                }

            });

    });
}

// Update jobsector
exports.update = function (req, res, next) {
    var jobsector = req.jobsector;

    // use save instead of update to trigger 'save' event for password hashing
    jobsector.set(req.body);
    jobsector.save(function (err, jobsector) {

        // Uniqueness and Save Validations

        if (err && err.code == 11001) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.redirect('/activejobfinder/web/jobsectors/account');
        }
        if (err) return next(err);

        // JobSector updated successfully, redirecting

        req.flash('success', "Account updated successfully.");
        return res.redirect('/activejobfinder/web/jobsectors/account');


    });
}

// Create jobsector
exports.create = function (req, res, next) {
    var newJobSector = new JobSector(req.body);
    newJobSector.jobsector_id = random.randomGUID();
//    , jobband_id: newJobSector.jobband_id
    JobSector.findOne({businesssector: newJobSector.businesssector}, function (err, jobSectorFromRepo) {
        if (!err) {
            if (jobSectorFromRepo) {
                req.flash('error', "Business Sector already defined");
                JobBand.find(function (err, jobbands) {
                    if (err) return next(err);
                    if (jobbands.length > 0) {
                        res.render('jobsectors/new', {jobsector: newJobSector, jobbands: jobbands, errorMessages: req.flash('error')});
                    } else {
                        req.flash('error', "Please add some Job Bands first.");
                        return res.redirect('/activejobfinder/web/users/dashboard');
                    }
                });
            }
            else {
                newJobSector.save(function (err, jobsector) {

                    // Uniqueness and save validations

                    if (err && err.code == 11000) {
                        var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
                        req.flash('error', "That " + duplicatedAttribute + " is already in use.");
                        return res.render('jobsectors/new', {jobsector: newJobSector, errorMessages: req.flash('error')});
                    }
                    if (err) return next(err);

                    req.flash('success', "JobSector Profile created successfully!");
                    return res.redirect('/activejobfinder/web/jobsectors');
                });
            }
        }
        else {
            req.flash('error', "Internal Server Error (" + err + ")");
            JobBand.find(function (err, jobbands) {
                if (err) return next(err);
                if (jobbands.length > 0) {
                    return res.render('jobsectors/new', {jobsector: newJobSector, jobbands: jobbands, errorMessages: req.flash('error')});
//                    return res.render('jobsectors/new', {jobsector: newJobSector, errorMessages: req.flash('error')});
                } else {
                    req.flash('error', "Please add some Job Bands first.");
                    return res.redirect('/activejobfinder/web/users/dashboard');
                }
            });
        }

    });
}

// Validations for jobsector objects upon jobsector update or create
exports.jobsectorValidations = function (req, res, next) {
    var creatingJobSector = req.url == "/activejobfinder/web/jobsectors/register";
    req.assert('jobsectorname', 'JobSectorname is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingJobSector) return res.render('jobsectors/new', {jobsector: new JobSector(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/jobsectors/account");
    } else next();
}
