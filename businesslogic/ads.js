var mongoose = require('mongoose')
    , Ad = require('../models/AdModel')//mongoose.model('Ad')
    , passport = require('passport')
    , random = require('../utilities/random');


// Get dashboard
exports.dashboard = function (req, res) {
    res.render('ads/dashboard');
}


// Get registration page
exports.register = function (req, res) {
    res.render('ads/new', {ad: new Ad({})});
}


// Account page
exports.account = function (req, res) {
    res.render('ads/edit');
}

// List all ads
exports.list = function (req, res, next) {
    Ad.find(function (err, ads) {
        if (err) return next(err);
        res.render('ads/index', {
            ads: ads
        });
    });
}

// Update ad
exports.update = function (req, res, next) {
    var ad = req.ad;

    // use save instead of update to trigger 'save' event for password hashing
    ad.set(req.body);
    ad.save(function (err, ad) {

        // Uniqueness and Save Validations

        if (err && err.code == 11001) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.redirect('/activejobfinder/web/ads/account');
        }
        if (err) return next(err);

        // Ad updated successfully, redirecting

        req.flash('success', "Account updated successfully.");
        return res.redirect('/activejobfinder/web/ads/account');

    });
}

// Create ad
exports.create = function (req, res, next) {
    var newAd = new Ad(req.body);
    newAd.ad_id = random.randomGUID();
    newAd.save(function (err, ad) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('ads/new', {ad: newAd, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "Ad Profile created successfully!");
        return res.redirect('/activejobfinder/web/ads');
    });
}

// Validations for ad objects upon ad update or create
exports.adValidations = function (req, res, next) {
    var creatingAd = req.url == "/activejobfinder/web/ads/register";
    req.assert('type', 'Job Type is required.').notEmpty();
    req.assert('description', 'Description is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingAd) return res.render('ads/new', {ad: new Ad(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/ads/account");
    } else next();
}
