var mongoose = require('mongoose')
    , JobBand = require('../models/JobBandModel')//mongoose.model('JobBand')
    , passport = require('passport')
    , random = require('../utilities/random');


// Get dashboard
exports.dashboard = function (req, res) {
    res.render('jobbands/dashboard');
}


// Get registration page
exports.register = function (req, res) {
    res.render('jobbands/new', {jobband: new JobBand({})});
}


// Account page
exports.account = function (req, res) {
    res.render('jobbands/edit');
}

// List all jobbands
exports.list = function (req, res, next) {
    JobBand.find(function (err, jobbands) {
        if (err) return next(err);
        res.render('jobbands/index', {
            jobbands: jobbands
        });
    });
}

// Update jobband
exports.update = function (req, res, next) {
    var jobband = req.jobband;

    // use save instead of update to trigger 'save' event for password hashing
    jobband.set(req.body);
    jobband.save(function (err, jobband) {

        // Uniqueness and Save Validations

        if (err && err.code == 11001) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.redirect('/activejobfinder/web/jobbands/account');
        }
        if (err) return next(err);

        // JobBand updated successfully, redirecting

        req.flash('success', "Account updated successfully.");
        return res.redirect('/activejobfinder/web/jobbands/account');

    });
}

// Create jobband
exports.create = function (req, res, next) {
    var newJobBand = new JobBand(req.body);
    newJobBand.jobband_id = random.randomGUID();
    newJobBand.save(function (err, jobband) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('jobbands/new', {jobband: newJobBand, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "JobBand Profile created successfully!");
        return res.redirect('/activejobfinder/web/jobbands');
    });
}

// Validations for jobband objects upon jobband update or create
exports.jobbandValidations = function (req, res, next) {
    var creatingJobBand = req.url == "/activejobfinder/web/jobbands/register";
    req.assert('jobbandname', 'JobBandname is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingJobBand) return res.render('jobbands/new', {jobband: new JobBand(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/jobbands/account");
    } else next();
}
