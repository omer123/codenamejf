var mongoose = require('mongoose')
    , JobType = require('../models/JobTypeModel')//mongoose.model('JobType')
    , passport = require('passport')
    , random = require('../utilities/random');


// Get dashboard
exports.dashboard = function (req, res) {
    res.render('jobtypes/dashboard');
}


// Get registration page
exports.register = function (req, res) {
    res.render('jobtypes/new', {jobtype: new JobType({})});
}


// Account page
exports.account = function (req, res) {
    res.render('jobtypes/edit');
}

// List all jobtypes
exports.list = function (req, res, next) {
    JobType.find(function (err, jobtypes) {
        if (err) return next(err);
        res.render('jobtypes/index', {
            jobtypes: jobtypes
        });
    });
}

// Update jobtype
exports.update = function (req, res, next) {
    var jobtype = req.jobtype;

    // use save instead of update to trigger 'save' event for password hashing
    jobtype.set(req.body);
    jobtype.save(function (err, jobtype) {

        // Uniqueness and Save Validations

        if (err && err.code == 11001) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.redirect('/activejobfinder/web/jobtypes/account');
        }
        if (err) return next(err);

        // JobType updated successfully, redirecting

        req.flash('success', "Account updated successfully.");
        return res.redirect('/activejobfinder/web/jobtypes/account');

    });
}

// Create jobtype
exports.create = function (req, res, next) {
    var newJobType = new JobType(req.body);
    newJobType.jobtype_id = random.randomGUID();
    newJobType.save(function (err, jobtype) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('jobtypes/new', {jobtype: newJobType, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "JobType Profile created successfully!");
        return res.redirect('/activejobfinder/web/jobtypes');
    });
}

// Validations for jobtype objects upon jobtype update or create
exports.jobtypeValidations = function (req, res, next) {
    var creatingJobType = req.url == "/activejobfinder/web/jobtypes/register";
    req.assert('type', 'Job Type is required.').notEmpty();
    req.assert('description', 'Description is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingJobType) return res.render('jobtypes/new', {jobtype: new JobType(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/jobtypes/account");
    } else next();
}
