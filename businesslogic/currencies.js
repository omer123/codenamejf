var mongoose = require('mongoose')
    , Currency = require('../models/CurrencyModel')//mongoose.model('Currency')
    , passport = require('passport')
    , random = require('../utilities/random');

// Get dashboard
exports.dashboard = function (req, res) {
    res.render('currencies/dashboard');
}

// Get registration page
exports.register = function (req, res) {
    res.render('currencies/new', {currency: new Currency({})});
}

// Account page
exports.account = function (req, res) {
    res.render('currencies/edit');
}

// List all currencies
exports.list = function (req, res, next) {
    Currency.find(function (err, currencies) {
        if (err) return next(err);
        res.render('currencies/index', {
            currencies: currencies
        });
    });
}

// Update currency
exports.update = function (req, res, next) {
    var currency = req.currency;

    // use save instead of update to trigger 'save' event for password hashing
    currency.set(req.body);
    currency.save(function (err, currency) {

        // Uniqueness and Save Validations

        if (err && err.code == 11001) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.redirect('/activejobfinder/web/currencies/account');
        }
        if (err) return next(err);

        // Currency updated successfully, redirecting

        req.flash('success', "Account updated successfully.");
        return res.redirect('/activejobfinder/web/currencies/account');


    });
}

// Create currency
exports.create = function (req, res, next) {
    var newCurrency = new Currency(req.body);
    newCurrency.currency_id = random.randomGUID();
    newCurrency.save(function (err, currency) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('currencies/new', {currency: newCurrency, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "Currency Profile created successfully!");
        return res.redirect('/activejobfinder/web/currencies');
    });
}

// Validations for currency objects upon currency update or create
exports.currencyValidations = function (req, res, next) {
    var creatingCurrency = req.url == "/activejobfinder/web/currencies/register";
    req.assert('currencyname', 'Currencyname is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingCurrency) return res.render('currencies/new', {currency: new Currency(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/currencies/account");
    } else next();
}