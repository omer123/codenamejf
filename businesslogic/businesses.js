var mongoose = require('mongoose')
    , Business = require('../models/BusinessModel')//mongoose.model('Business')
    , passport = require('passport')
    , random = require('../utilities/random');


// Get dashboard
exports.dashboard = function (req, res) {
    res.render('businesses/dashboard');
}


// Get registration page
exports.register = function (req, res) {
    res.render('businesses/new', {business: new Business({})});
}


// Account page
exports.account = function (req, res) {
    res.render('businesses/edit');
}

// List all businesses
exports.list = function (req, res, next) {
    Business.find(function (err, businesses) {
        if (err) return next(err);
        res.render('businesses/index', {
            businesses: businesses
        });
    });
}

// Update business
exports.update = function (req, res, next) {
    var business = req.business;

    // use save instead of update to trigger 'save' event for password hashing
    business.set(req.body);
    business.save(function (err, business) {

        // Uniqueness and Save Validations

        if (err && err.code == 11001) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.redirect('/activejobfinder/web/businesses/account');
        }
        if (err) return next(err);

        // Business updated successfully, redirecting

        req.flash('success', "Account updated successfully.");
        return res.redirect('/activejobfinder/web/businesses/account');


    });
}

// Create business
exports.create = function (req, res, next) {
    var newBusiness = new Business(req.body);
    newBusiness.business_id = random.randomGUID();
    newBusiness.save(function (err, business) {

        // Uniqueness and save validations

        if (err && err.code == 11000) {
            var duplicatedAttribute = err.err.split("$")[1].split("_")[0];
            req.flash('error', "That " + duplicatedAttribute + " is already in use.");
            return res.render('businesses/new', {business: newBusiness, errorMessages: req.flash('error')});
        }
        if (err) return next(err);

        req.flash('success', "Business Profile created successfully!");
        return res.redirect('/activejobfinder/web/businesses');
    });
}

// Validations for business objects upon business update or create
exports.businessValidations = function (req, res, next) {
    var creatingBusiness = req.url == "/activejobfinder/web/businesses/register";
    req.assert('businessname', 'Businessname is required.').notEmpty();

    var validationErrors = req.validationErrors() || [];
    if (validationErrors.length > 0) {
        validationErrors.forEach(function (e) {
            req.flash('error', e.msg);
        });
        // Create handling if errors present
        if (creatingBusiness) return res.render('businesses/new', {business: new Business(req.body), errorMessages: req.flash('error')});
        // Update handling if errors present
        else return res.redirect("/activejobfinder/web/businesses/account");
    } else next();
}
